# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 2.21.0

- Fix Microprofile Config: Don't throw errors if client is disabled [#22](https://gitlab.com/configseeder/java-clients/issues/22)

## 2.18.1

- SmallRye Config supports now setting priority over `configseeder.client.config-source.priority` 

## 2.18.0

- Support SmallRye Config

## 2.11.0

- Update to Eclipse MicroProfile Config 1.4
- Add support for CERTIFICATE and PRIVATE_KEY (just string value support)


## 2.10.0

- Support for fallback when Config Seeder Management is not available. Use Option `configseeder.client.recoveryFile` which can be either a directory or a concrete file. [#16](https://gitlab.com/configseeder/java-clients/issues/16)

## 2.9.0

- Old timer not removed after creating a new one. [#19](https://gitlab.com/configseeder/java-clients/issues/19)


## 2.8.1

- Decrease number of requests against ConfigSeeder. [#18](https://gitlab.com/configseeder/java-clients/issues/18)


## 2.8.0

### BREAKING CHANGE

Client from Version 2.8.0 need at least a ConfigSeeder Management in Version 2.8.0.

- Use new public and versionized api released on /public/api/v1/ [#17](https://gitlab.com/configseeder/java-clients/issues/17)

## 2.7.0

- Support for REGEX data type


## 2.6.1

### Fixed

- Close HttpClient at the end of each call [#15](https://gitlab.com/configseeder/java-clients/issues/15)

## 2.6.0

### Added

- Support for regex

## 2.5.6

### Fixed

- Fix getInteger() call on Java API [#13](https://gitlab.com/configseeder/java-clients/issues/13) 


## 2.5.4

### Fixed

- Eclipse Micro Profile version for Quarkus with only one ConfigSeeder Client Instance [#12](https://gitlab.com/configseeder/java-clients/issues/12)


## 2.5.3

### Fixed

- Eclipse Micro Profile version does not work for Quarkus: Endless loop [#11](https://gitlab.com/configseeder/java-clients/issues/11)


## 2.5.1

### Changed

- Add support for REGEX type (will be added in ConfigSeeder Management Version 2.6.x)


## 2.4.1

### Changed

- Add example based on Quarkus
- Update keys and make demo applications work with the latest ConfigSeeder release
- Fix License
- Add README.md for each of the examples


## 2.3.0

### Changed

- Support Zoned Date Time [#9](https://gitlab.com/configseeder/server/issues/9)


## 2.2.0

### Changed

- Add identification for each client [#9](https://gitlab.com/configseeder/java-clients/issues/9)

## 2.0.3

### Fixed

- Fix current time not sent [#8](https://gitlab.com/configseeder/java-clients/issues/8)


## 2.0.1

### Fixed

- Fix configurationGroupKeys with space [#7](https://gitlab.com/configseeder/java-clients/issues/7)


## 2.0.0

2.0.0 is **not compatible** with 1.x!

### Changed

- Config Client log to System.out if Slf4j is not available [#1](https://gitlab.com/configseeder/java-clients/issues/1)
- Notification for deleted key in ConfigClient [#2](https://gitlab.com/configseeder/java-clients/issues/2)
- Client only throws `InvalidClientConfigurationException`

### Fixed

- Propagate exception from server side [#3](https://gitlab.com/configseeder/java-clients/issues/3)


## 1.6.3

### Updated

- Updated Eclipse Microprofile Config from Version 1.2 to Version 1.3


## 1.6.2

### Fixed

- Remove spring dependencies from core [#4](https://gitlab.com/configseeder/java-clients/issues/4)
