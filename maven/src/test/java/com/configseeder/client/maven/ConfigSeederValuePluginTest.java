/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client.maven;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.configseeder.client.model.VersionedConfigurationGroupSelectionMode;
import com.github.tomakehurst.wiremock.WireMockServer;
import java.util.Properties;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class ConfigSeederValuePluginTest {

    private static final WireMockServer wireMockServer = new WireMockServer(0);

    @BeforeAll
    static void startServer() {
        // given
        wireMockServer.stubFor(post(urlEqualTo("/public/api/v1/configurations"))
                                     .willReturn(aResponse()
                                                         .withStatus(200)
                                                         .withHeader("Content-Type", "application/json")
                                                         .withBody(
                                                                 "[{\"key\": \"foo.bar\", \"value\": \"any text\", \"type\": \"STRING\", "
                                                                         + "\"lastUpdate\":\"2018-01-03T17:36:01\", \"lastUpdateInMilliseconds\": 125455545445}]")
                                                )
                              );
        wireMockServer.start();
    }

    @AfterAll
    static void stopServer() {
        wireMockServer.stop();
    }

    @Test
    void shouldFetchValuesFromServer() throws MojoFailureException {
        // given
        final Properties properties = new Properties();
        final MavenProject mavenProject = mock(MavenProject.class);
        when(mavenProject.getProperties()).thenReturn(properties);

        final ConfigurationGroupMavenConfiguration configurationGroup = new ConfigurationGroupMavenConfiguration();
        configurationGroup.setKey("cg1");
        configurationGroup.setSelectionMode(VersionedConfigurationGroupSelectionMode.LATEST);

        final ConfigSeederValuePlugin configSeederValuePlugin = new ConfigSeederValuePlugin();
        configSeederValuePlugin.setProject(mavenProject);
        configSeederValuePlugin.setServerUrl("http://localhost:" + wireMockServer.port());
        configSeederValuePlugin.setApiKey("api-key");
        configSeederValuePlugin.setTenant("tenant-key");
        configSeederValuePlugin.setEnvironment("environment");
        configSeederValuePlugin.setConfigurationGroups(singletonList(configurationGroup));

        // when
        configSeederValuePlugin.execute();

        // then
        assertThat(properties).hasSize(1);
        assertThat(properties.getProperty("foo.bar")).isEqualTo("any text");
    }

}