/*
 * Copyright (c) 2021 Oneo GmbH (ConfigSeeder®, https://configseeder.com/) and others.
 *
 * Any form of reproduction, distribution, exploitation or alteration is prohibited
 * without the prior written consent of Oneo GmbH.
 */

package com.configseeder.client.smallryeconfig;

import com.configseeder.client.ConfigSeederClient;
import com.configseeder.client.microprofileconfig.ConfigSeederBaseConfigSource;
import java.io.Closeable;

/**
 * MicroProfile Config Source that is backed by ConfigSeeder
 * <p>
 * The Config Source itself needs configuration which is handled by other Config Sources.
 */
public class ConfigSeederSmallRyeConfigSource extends ConfigSeederBaseConfigSource implements Closeable {

    // Name of this ConfigSource
    private static final String CONFIGSEEDER_CONFIG_SOURCE_NAME = "com.configseeder.client.configsource.smallrye";

    private final ConfigSeederClient configSeederClient;

    /**
     * Initialization of ConfigSeeder Config Source
     */
    ConfigSeederSmallRyeConfigSource(ConfigSeederClient configSeederClient, Integer priority) {
        super(CONFIGSEEDER_CONFIG_SOURCE_NAME, priority);
        this.configSeederClient = configSeederClient;
    }

    protected ConfigSeederClient getConfigSeederClient() {
        return configSeederClient;
    }

    @Override
    public void close() {
        configSeederClient.shutdown();
    }

}
