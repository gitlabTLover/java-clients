/*
 * Copyright (c) 2021 Oneo GmbH (ConfigSeeder®, https://configseeder.com/) and others.
 *
 * Any form of reproduction, distribution, exploitation or alteration is prohibited
 * without the prior written consent of Oneo GmbH.
 */

package com.configseeder.client.smallryeconfig;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import com.github.tomakehurst.wiremock.WireMockServer;
import io.smallrye.config.ConfigSourceContext;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.spi.ConfigProviderResolver;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class ConfigSeederSmallRyeConfigSourceTest {

    private static WireMockServer wireMockServer;
    private ConfigSeederSmallRyeConfigSource configSource;

    @BeforeAll
    static void startServer() {
        // given
        wireMockServer = new WireMockServer(8091);
        wireMockServer.stubFor(post(urlEqualTo("/public/api/v1/configurations"))
                                     .willReturn(aResponse()
                                                         .withStatus(200)
                                                         .withHeader("Content-Type", "application/json")
                                                         .withBody("[{\"key\": \"foo.bar\", \"value\": \"any text\", \"type\": \"STRING\", "
                                                                           + "\"lastUpdate\":\"2018-01-03T17:36:01\", "
                                                                           + "\"lastUpdateInMilliseconds\": 101837483273}]")
                                                )
                            );
        wireMockServer.start();
    }

    @AfterEach
    void shutdownAccess() {
        if (configSource != null) {
            configSource.close();
            configSource = null;
        }
    }

    @AfterAll
    static void stopServer() {
        wireMockServer.stop();
    }

    @Test
    void shouldLoadValueWithoutMicroProfile() {
        // given
        ConfigSourceContext configSourceContext = mock(ConfigSourceContext.class);
        configSource = (ConfigSeederSmallRyeConfigSource) new ConfigSeederSmallRyeConfigSourceFactory().getConfigSources(configSourceContext).iterator().next();

        // when
        final String value = configSource.getValue("foo.bar");

        // then
        assertThat(value).isEqualTo("any text");
    }

    @Test
    void shouldProvideValueMicroProfileWithoutSpi() {
        // given
        final Config config = ConfigProviderResolver.instance().getBuilder()
                .addDefaultSources()
                .addDiscoveredSources()
                .build();

        // when
        final String value = config.getValue("foo.bar", String.class);

        // then
        assertThat(value).isEqualTo("any text");
    }

}
