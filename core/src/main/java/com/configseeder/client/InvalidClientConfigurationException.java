/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client;

public class InvalidClientConfigurationException extends IllegalArgumentException {

    /**
     * C'tor.
     *
     * @param technicalMessage technical message
     */
    public InvalidClientConfigurationException(String technicalMessage) {
        super(technicalMessage);
    }

    /**
     * Exception with just a technical message.
     *
     * @param technicalMessage technical message
     * @param throwable cause
     */
    public InvalidClientConfigurationException(String technicalMessage, Throwable throwable) {
        super(technicalMessage, throwable);
    }

}
