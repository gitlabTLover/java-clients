/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client.model;

import com.configseeder.client.InvalidClientConfigurationException;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;

/**
 * Configuration request.
 */
@Data
@Builder
@AllArgsConstructor
public class ConfigurationRequest {

    /** Default tenant key. */
    public static final String DEFAULT_TENANT_KEY = "default";

    /** Tenant key is optional, fallback to "default". */
    @Default
    private String tenantKey = DEFAULT_TENANT_KEY;

    /** Environment key is mandatory. */
    private String environmentKey;

    /** Context is optional. */
    private String context;

    /** Version is optional. */
    private String version;

    /** Date time is optional. */
    private LocalDateTime dateTime;

    @Default
    private List<ConfigurationEntryRequest> configurations = new LinkedList<>();

    /**
     * C'tor for serialization.
     */
    public ConfigurationRequest() {
        this.configurations = new LinkedList<>();
    }

    /**
     * Validates the DTO.
     */
    public void validate() {
        if (tenantKey == null) {
            throw new InvalidClientConfigurationException("tenantKey should never be null");
        }
        if (environmentKey != null && environmentKey.trim().length() == 0) {
            throw new InvalidClientConfigurationException("environment should either be null or have a value (length > 0)");
        }
        if (configurations == null || configurations.isEmpty()) {
            throw new InvalidClientConfigurationException("configurations should have at least one element");
        }
        int i = 0;
        for (ConfigurationEntryRequest configurationGroup : configurations) {
            configurationGroup.validate(i++);
        }
    }

}
