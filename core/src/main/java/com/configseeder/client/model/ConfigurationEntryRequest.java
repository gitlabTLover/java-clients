/*
 * Copyright (c) 2021 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client.model;

import com.configseeder.client.InvalidClientConfigurationException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Represents a configuration request.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConfigurationEntryRequest {

    private String versionedConfigurationGroupId;
    private String configurationGroupId;
    private String configurationGroupKey;
    private Integer configurationGroupVersionNumber; // can be empty
    private VersionedConfigurationGroupSelectionMode selectionMode; // can be empty

    /**
     * validates the entry.
     *
     * @param index index in the list
     */
    void validate(int index) {
        if (versionedConfigurationGroupId != null) {
            return;
        }
        if (configurationGroupId != null) {
            return;
        }
        if (configurationGroupKey == null || configurationGroupKey.trim().length() == 0) {
            throw new InvalidClientConfigurationException("Field configurationGroupKey at index " + index + " should not be null or empty");
        }
        if (configurationGroupVersionNumber != null //
                && (selectionMode != null && !VersionedConfigurationGroupSelectionMode.VERSION_NUMBER.equals(selectionMode))) {
            throw new InvalidClientConfigurationException("Search by version number should not have selection mode set at index " + index);
        }
    }

}
