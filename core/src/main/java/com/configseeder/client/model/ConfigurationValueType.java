/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client.model;

/**
 * Different type of configuration values.
 */
public enum ConfigurationValueType {

    /** Single line string. */
    STRING,

    /** String with multiline character. */
    MULTILINE_STRING,

    /** Support for regular expressions. */
    REGEX,

    /** Support for enumerations. */
    ENUM,

    /** Textual representation of an integer. */
    INTEGER,

    /** Textual representation of any number. */
    NUMBER,

    /** Textual representation of a boolean. */
    BOOLEAN,

    /** ConfigurationGroupId stored as comma separated string. */
    CONFIGURATION_GROUP,

    /** API Key Type. */
    API_KEY_TYPE,

    /** Binary datatype. */
    BINARY,

    /** PEM Formatted (TLS) Certificate. */
    CERTIFICATE,

    /** PEM Formatted (TLS) Private Key. */
    PRIVATE_KEY;

    /*,

    DATE,

    DATETIME,

    TIME,

    DURATION,

    URI,

    EMAIL,

    JSON
*/

    /**
     * Safe read of a configuration value type with fallback.
     */
    public static ConfigurationValueType safeValueOf(String value, ConfigurationValueType fallback) {
        try {
            return ConfigurationValueType.valueOf(value);
        } catch (IllegalArgumentException ex) {
            return fallback;
        }
    }

}
