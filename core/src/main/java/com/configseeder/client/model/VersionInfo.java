/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client.model;

import java.io.InputStream;
import java.util.Scanner;
import lombok.Data;

@Data
public class VersionInfo {

    /**
     * Get version info from file.
     * @return version info
     */
    public static VersionInfo fromFile() {
        final InputStream inputStream = VersionInfo.class.getResourceAsStream("/configseeder-client.version");
        if (inputStream == null) {
            throw new IllegalArgumentException("Could not found version file");
        }
        Scanner s = new Scanner(inputStream).useDelimiter("\\A");
        return new VersionInfo(s.hasNext() ? s.next() : "");
    }

    private final String version;

}
