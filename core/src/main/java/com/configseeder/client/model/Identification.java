/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client.model;

import com.configseeder.utils.IdentificationUtil;
import com.configseeder.utils.ShaDigestUtils;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Identification {

    public static final String HEADER_X_USER_AGENT = "X-User-Agent";
    public static final String HEADER_X_HOSTNAME = "X-Hostname";
    public static final String HEADER_X_HOST_IDENTIFICATION = "X-Host-Identification";

    /**
     * Create identification object based on type.
     *
     * @param clientType client type (e.g. Core, Spring, Maven, EclipseMicroProfileConfig)
     * @return identification
     */
    public static Identification create(String clientType) {
        final VersionInfo versionInfo = VersionInfo.fromFile();
        final String operatingSystemDetails = IdentificationUtil.getOperatingSystemDetails();
        final String hostname = IdentificationUtil.getHostname();
        final String hwIdentification = IdentificationUtil.getIdentificationBasedOnHardware();
        return Identification.builder()
                .userAgent("ConfigSeederClient-" + clientType + "/" + versionInfo.getVersion() + " (" + operatingSystemDetails + ")")
                .hostname(hostname == null ? "unknown" : hostname)
                .hostIdentification(ShaDigestUtils.shaHex(1, hwIdentification == null ? UUID.randomUUID().toString() : hwIdentification))
                .build();
    }

    private String userAgent;
    private String hostname;
    private String hostIdentification;

}
