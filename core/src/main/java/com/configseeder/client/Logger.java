/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client;

import org.slf4j.LoggerFactory;

/**
 * Logger factory.
 */
public class Logger {

    private final org.slf4j.Logger slf4jLogger;

    Logger(Class<?> clazz) {
        Class<?> loggerClazz;
        try {
            loggerClazz = Class.forName("org.slf4j.Logger");
        } catch (ClassNotFoundException e) {
            loggerClazz = null;
        }
        slf4jLogger = loggerClazz == null ? null : LoggerFactory.getLogger(clazz);
    }

    public static Logger getLogger(Class<?> clazz) {
        return new Logger(clazz);
    }

    public void debug(String message, Object... parameters) {
        if (slf4jLogger != null) {
            slf4jLogger.debug(message, parameters);
        } else {
            System.out.println("DEBUG: " + String.format(toStringFormat(message), parameters)); // NOSONAR
        }
    }

    public void debug(String message, Throwable t) {
        if (slf4jLogger != null) {
            slf4jLogger.debug(message, t);
        } else {
            System.out.println("DEBUG: " + message); // NOSONAR
            t.printStackTrace(System.out); // NOSONAR
        }
    }

    public void info(String message, Object... parameters) {
        if (slf4jLogger != null) {
            slf4jLogger.info(message, parameters);
        } else {
            System.out.println("INFO: " + String.format(toStringFormat(message), parameters)); // NOSONAR
        }
    }

    public void info(String message, Throwable t) {
        if (slf4jLogger != null) {
            slf4jLogger.info(message, t);
        } else {
            System.out.println("INFO: " + message); // NOSONAR
            t.printStackTrace(System.out); // NOSONAR
        }
    }

    public void warn(String message, Object... parameters) {
        if (slf4jLogger != null) {
            slf4jLogger.warn(message, parameters);
        } else {
            System.out.println("WARN: " + String.format(toStringFormat(message), parameters)); // NOSONAR
        }
    }

    public void warn(String message, Throwable t) {
        if (slf4jLogger != null) {
            slf4jLogger.warn(message, t);
        } else {
            System.out.println("WARN: " + message); // NOSONAR
            t.printStackTrace(System.out); // NOSONAR
        }
    }

    public void error(String message, Object... parameters) {
        if (slf4jLogger != null) {
            slf4jLogger.error(message, parameters);
        } else {
            System.out.println("ERROR: " + String.format(toStringFormat(message), parameters)); // NOSONAR
        }
    }

    public void error(String message, Throwable t) {
        if (slf4jLogger != null) {
            slf4jLogger.error(message, t);
        } else {
            System.err.println("ERROR: " + message); // NOSONAR
            t.printStackTrace(System.err); // NOSONAR
        }
    }

    private static String toStringFormat(String message) {
        return message == null ? null : message.replace("{}", "%s");
    }

}
