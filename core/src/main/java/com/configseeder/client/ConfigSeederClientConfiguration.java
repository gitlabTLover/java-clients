/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client;

import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableList;

import com.configseeder.client.model.ConfigurationRequest;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.ToString;

/**
 * Configuration client configuration.
 */
@Data
@Builder
@ToString(exclude = "apiKey")
public class ConfigSeederClientConfiguration {

    /** Default initialization mode. */
    public static final InitializationMode DEFAULT_INITIALIZATION_MODE = InitializationMode.EAGER_ASYNC;
    /** Default refresh mode. */
    public static final RefreshMode DEFAULT_REFRESH_MODE = RefreshMode.TIMER;
    /** Default refresh cycle in milliseconds. */
    public static final Integer DEFAULT_REFRESH_CYCLE = 60000;
    /** Default max retries after failing the synchronization. */
    public static final Integer DEFAULT_MAX_RETRIES = 3;
    /** Default retry wait time after failure in milliseconds. */
    public static final Integer DEFAULT_RETRY_WAIT_TIME = 1000;
    /** Default connection timeout in milliseconds. */
    public static final Integer DEFAULT_CONNECTION_TIMEOUT = 1000;
    /** Default read timeout in milliseconds. */
    public static final Integer DEFAULT_READ_TIMEOUT = 2000;

    /** CONFIG_SEEDER_CLIENT_PREFIX. */
    private static final String PREFIX = "configseeder.client.";
    /** Config Key for Config Seeder Server URL. */
    public static final String CONFIGSEEDER_CLIENT_SERVERURL = PREFIX + "serverUrl";
    /** Config Key for API Key. */
    public static final String CONFIGSEEDER_CLIENT_APIKEY = PREFIX + "apiKey";
    /** Config Key for Tenant Key. */
    public static final String CONFIGSEEDER_CLIENT_TENANTKEY = PREFIX + "tenantKey";
    /** Config Key for Context Filter. */
    public static final String CONFIGSEEDER_CLIENT_CONTEXT = PREFIX + "context";
    /** Config Key for Date Time Filter. */
    public static final String CONFIGSEEDER_CLIENT_DATETIME = PREFIX + "dateTime";
    /** Config Key for Version Filter. */
    public static final String CONFIGSEEDER_CLIENT_VERSION = PREFIX + "version";
    /** Config Key for Environment Filter. */
    public static final String CONFIGSEEDER_CLIENT_ENVIRONMENTKEY = PREFIX + "environmentKey";
    /** Config Key for Configuration Filter (simple config). */
    public static final String CONFIGSEEDER_CLIENT_CONFIGURATIONGROUPKEYS = PREFIX + "configurationGroupKeys";
    /** Config Key for Configuration Filter. */
    public static final String CONFIGSEEDER_CLIENT_CONFIGURATIONS = PREFIX + "configurations";
    /** Config Key for Connection Timeout. */
    public static final String CONFIGSEEDER_CLIENT_CONNECTIONTIMEOUT = PREFIX + "connectionTimeout";
    /** Config Key for Read Timeout. */
    public static final String CONFIGSEEDER_CLIENT_READTIMEOUT = PREFIX + "readTimeout";
    /** Config Key for Max Retries. */
    public static final String CONFIGSEEDER_CLIENT_MAXRETRIES = PREFIX + "maxRetries";
    /** Config Key for Retry Wait Time. */
    public static final String CONFIGSEEDER_CLIENT_RETRYWAITTIME = PREFIX + "retryWaitTime";
    /** Config Key for refresh cycle. */
    public static final String CONFIGSEEDER_CLIENT_REFRESHCYCLE = PREFIX + "refreshCycle";
    /** Config Key for Refresh mode of config seeder client. */
    public static final String CONFIGSEEDER_CLIENT_REFRESHMODE = PREFIX + "refreshMode";
    /** Config Key for Initialization mode of config seeder client. */
    public static final String CONFIGSEEDER_CLIENT_INITIALIZATIONMODE = PREFIX + "initializationMode";
    /** Config Key for Disable configseeder client. */
    public static final String CONFIGSEEDER_CLIENT_DISABLED = PREFIX + "disabled";
    /** Config Key for recovery file path. */
    public static final String CONFIGSEEDER_CLIENT_RECOVERY_FILE = PREFIX + "recoveryFile";

    /** All configurations. */
    public static final List<String> ALL_CONFIG_KEYS = unmodifiableList(asList(
            CONFIGSEEDER_CLIENT_SERVERURL,
            CONFIGSEEDER_CLIENT_APIKEY,
            CONFIGSEEDER_CLIENT_TENANTKEY,
            CONFIGSEEDER_CLIENT_CONTEXT,
            CONFIGSEEDER_CLIENT_DATETIME,
            CONFIGSEEDER_CLIENT_VERSION,
            CONFIGSEEDER_CLIENT_ENVIRONMENTKEY,
            CONFIGSEEDER_CLIENT_CONFIGURATIONGROUPKEYS,
            CONFIGSEEDER_CLIENT_CONFIGURATIONS,
            CONFIGSEEDER_CLIENT_CONNECTIONTIMEOUT,
            CONFIGSEEDER_CLIENT_READTIMEOUT,
            CONFIGSEEDER_CLIENT_MAXRETRIES,
            CONFIGSEEDER_CLIENT_RETRYWAITTIME,
            CONFIGSEEDER_CLIENT_REFRESHCYCLE,
            CONFIGSEEDER_CLIENT_REFRESHMODE,
            CONFIGSEEDER_CLIENT_INITIALIZATIONMODE,
            CONFIGSEEDER_CLIENT_DISABLED,
            CONFIGSEEDER_CLIENT_RECOVERY_FILE
    ));

    private String serverUrl;
    private String apiKey;

    @Default
    private boolean disabled = false;
    @Default
    private InitializationMode initializationMode = DEFAULT_INITIALIZATION_MODE;
    @Default
    private RefreshMode refreshMode = DEFAULT_REFRESH_MODE;
    @Default
    private Integer refreshCycle = DEFAULT_REFRESH_CYCLE;
    @Default
    private Integer maxRetries = DEFAULT_MAX_RETRIES;
    @Default
    private Integer retryWaitTime = DEFAULT_RETRY_WAIT_TIME;
    @Default
    private Integer connectionTimeout = DEFAULT_CONNECTION_TIMEOUT;
    @Default
    private Integer readTimeout = DEFAULT_READ_TIMEOUT;
    @Default
    private String userAgent = "ConfigSeederClient-Core";
    @Default
    private String recoveryFile = null;

    private ConfigurationRequest requestConfiguration;

    /**
     * Read configurations from properties file.
     *
     * @param propertiesFile properties file
     * @return configuration
     */
    public static ConfigSeederClientConfiguration fromProperties(String propertiesFile) {
        if (propertiesFile == null || propertiesFile.trim().length() == 0) {
            throw new InvalidClientConfigurationException("Configuration file path cannot be empty");
        }
        final InputStream propertiesFileStream = ConfigSeederClientConfiguration.class.getResourceAsStream(propertiesFile.trim());
        if (propertiesFileStream == null) {
            throw new InvalidClientConfigurationException("Could not find configuration located at '" + propertiesFile + "'");
        }
        return fromProperties(propertiesFileStream);
    }

    /**
     * Read configurations from properties file.
     *
     * @param inputStream properties file
     * @return configuration
     */
    public static ConfigSeederClientConfiguration fromProperties(InputStream inputStream) {
        return ConfigSeederClientConfigurationReader.fromProperties(inputStream);
    }

    /**
     * Read configurations from yaml file.
     *
     * @param yamlFile yaml file
     * @return configuration
     */
    public static ConfigSeederClientConfiguration fromYaml(String yamlFile) {
        if (yamlFile == null || yamlFile.trim().length() == 0) {
            throw new InvalidClientConfigurationException("Configuration file path cannot be empty");
        }
        final InputStream yamlFileStream = ConfigSeederClientConfiguration.class.getResourceAsStream(yamlFile.trim());
        if (yamlFileStream == null) {
            throw new InvalidClientConfigurationException("Could not find configuration located at '" + yamlFile + "'");
        }
        return fromYaml(yamlFileStream);
    }

    /**
     * Read configurations from yaml file.
     *
     * @param inputStream yaml file
     * @return configuration
     */
    public static ConfigSeederClientConfiguration fromYaml(InputStream inputStream) {
        return ConfigSeederClientConfigurationReader.fromYaml(inputStream);
    }

    /**
     * Create configuration from any map.
     *
     * @param config map with properties
     * @return config
     */
    public static ConfigSeederClientConfiguration fromMap(Map<Object, Object> config) {
        return ConfigSeederClientConfigurationReader.fromMap(config);
    }

    /**
     * Validates the configuration.
     */
    public void validate() {
        if (disabled) {
            return;
        }
        if (initializationMode == null) {
            throw new InvalidClientConfigurationException("initialization mode should not be null");
        }
        if (refreshMode == null) {
            throw new InvalidClientConfigurationException("refreshMode should not be null");
        }
        if (!RefreshMode.MANUAL.equals(refreshMode) && refreshCycle == null) {
            throw new InvalidClientConfigurationException("refreshCycle not defined");
        }
        if (serverUrl == null || !serverUrl.startsWith("http")) {
            throw new InvalidClientConfigurationException("serverUrl not well defined");
        }
        if (maxRetries == null) {
            throw new InvalidClientConfigurationException("maxRetries not defined");
        }
        if (retryWaitTime == null) {
            throw new InvalidClientConfigurationException("retryWaitTime not defined");
        }
        if (apiKey == null) {
            throw new InvalidClientConfigurationException("apiKey not defined");
        }
        if (connectionTimeout == null) {
            throw new InvalidClientConfigurationException("connectionTimeout unknown");
        }
        if (readTimeout == null) {
            throw new InvalidClientConfigurationException("readTimeout unknown");
        }
        if (requestConfiguration == null) {
            throw new InvalidClientConfigurationException("requestConfiguration not defined");
        }
        requestConfiguration.validate();
    }

}
