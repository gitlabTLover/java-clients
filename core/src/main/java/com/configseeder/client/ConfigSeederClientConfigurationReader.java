/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client;

import static com.configseeder.client.ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_APIKEY;
import static com.configseeder.client.ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_CONFIGURATIONGROUPKEYS;
import static com.configseeder.client.ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_CONFIGURATIONS;
import static com.configseeder.client.ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_CONNECTIONTIMEOUT;
import static com.configseeder.client.ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_CONTEXT;
import static com.configseeder.client.ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_DATETIME;
import static com.configseeder.client.ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_DISABLED;
import static com.configseeder.client.ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_ENVIRONMENTKEY;
import static com.configseeder.client.ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_INITIALIZATIONMODE;
import static com.configseeder.client.ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_MAXRETRIES;
import static com.configseeder.client.ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_READTIMEOUT;
import static com.configseeder.client.ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_RECOVERY_FILE;
import static com.configseeder.client.ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_REFRESHCYCLE;
import static com.configseeder.client.ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_REFRESHMODE;
import static com.configseeder.client.ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_RETRYWAITTIME;
import static com.configseeder.client.ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_SERVERURL;
import static com.configseeder.client.ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_TENANTKEY;
import static com.configseeder.client.ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_VERSION;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import com.configseeder.client.ConfigSeederClientConfiguration.ConfigSeederClientConfigurationBuilder;
import com.configseeder.client.model.ConfigurationEntryRequest;
import com.configseeder.client.model.ConfigurationRequest;
import com.configseeder.client.model.VersionedConfigurationGroupSelectionMode;
import com.configseeder.client.util.ParameterUtil;
import com.configseeder.shared.converter.YamlToPropertiesConverter;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Reads configuration model.
 */
class ConfigSeederClientConfigurationReader {

    private ConfigSeederClientConfigurationReader() {

    }


    /**
     * Reads configuration model from yaml and system parameters.
     *
     * @param yamlInputStream yaml input stream
     * @return configuration
     */
    static ConfigSeederClientConfiguration fromYaml(InputStream yamlInputStream) {
        if (yamlInputStream == null) {
            throw new InvalidClientConfigurationException("Could not find configuration");
        }
        final Properties properties = new YamlToPropertiesConverter().convert(yamlInputStream);
        return fromMap(properties);
    }

    /**
     * Reads configuration model from properties and system parameters.
     *
     * @param propertiesInputStream properties input stream
     * @return configuration
     */
    static ConfigSeederClientConfiguration fromProperties(InputStream propertiesInputStream) {
        try {
            if (propertiesInputStream == null) {
                throw new InvalidClientConfigurationException("Could not find configuration");
            }
            final Properties properties = new Properties();
            properties.load(propertiesInputStream);
            return fromMap(properties);
        } catch (IOException e) {
            throw new InvalidClientConfigurationException("Could not load configuration", e);
        }
    }

    /**
     * Reads configuration from different properties sources.
     *
     * @param properties properties
     * @return configuration
     */
    static ConfigSeederClientConfiguration fromMap(Map<Object, Object> properties) {
        final ConfigSeederClientConfigurationBuilder configBuilder = ConfigSeederClientConfiguration.builder();
        configBuilder.disabled(
                fetchConfig(properties, CONFIGSEEDER_CLIENT_DISABLED, Boolean.FALSE, Boolean::valueOf));
        configBuilder.initializationMode(
                fetchConfig(properties, CONFIGSEEDER_CLIENT_INITIALIZATIONMODE, InitializationMode.EAGER_ASYNC, InitializationMode::valueOf));
        configBuilder.refreshMode(
                fetchConfig(properties, CONFIGSEEDER_CLIENT_REFRESHMODE, RefreshMode.TIMER, RefreshMode::valueOf));
        configBuilder.serverUrl(
                fetchConfig(properties, CONFIGSEEDER_CLIENT_SERVERURL));
        configBuilder.apiKey(
                fetchConfig(properties, CONFIGSEEDER_CLIENT_APIKEY));
        configBuilder.refreshCycle(
                fetchIntConfig(properties, CONFIGSEEDER_CLIENT_REFRESHCYCLE, ConfigSeederClientConfiguration.DEFAULT_REFRESH_CYCLE));
        configBuilder.retryWaitTime(
                fetchIntConfig(properties, CONFIGSEEDER_CLIENT_RETRYWAITTIME, ConfigSeederClientConfiguration.DEFAULT_RETRY_WAIT_TIME));
        configBuilder.maxRetries(
                fetchIntConfig(properties, CONFIGSEEDER_CLIENT_MAXRETRIES, ConfigSeederClientConfiguration.DEFAULT_MAX_RETRIES));
        configBuilder.connectionTimeout(
                fetchIntConfig(properties, CONFIGSEEDER_CLIENT_CONNECTIONTIMEOUT, ConfigSeederClientConfiguration.DEFAULT_CONNECTION_TIMEOUT));
        configBuilder.readTimeout(
                fetchIntConfig(properties, CONFIGSEEDER_CLIENT_READTIMEOUT, ConfigSeederClientConfiguration.DEFAULT_READ_TIMEOUT));
        configBuilder.recoveryFile(
                fetchConfig(properties, CONFIGSEEDER_CLIENT_RECOVERY_FILE, null));

        final ConfigurationRequest.ConfigurationRequestBuilder configDtoBuilder = ConfigurationRequest.builder();

        configDtoBuilder.environmentKey(
                fetchConfig(properties, CONFIGSEEDER_CLIENT_ENVIRONMENTKEY));
        configDtoBuilder.tenantKey(
                fetchConfig(properties, CONFIGSEEDER_CLIENT_TENANTKEY, ConfigurationRequest.DEFAULT_TENANT_KEY));
        configDtoBuilder.version(
                fetchConfig(properties, CONFIGSEEDER_CLIENT_VERSION));
        configDtoBuilder.context(
                fetchConfig(properties, CONFIGSEEDER_CLIENT_CONTEXT));
        configDtoBuilder.dateTime(
                fetchConfig(properties, CONFIGSEEDER_CLIENT_DATETIME, null, LocalDateTime::parse));

        final String[] configGroupsShortVersion = fetchConfig(properties, CONFIGSEEDER_CLIENT_CONFIGURATIONGROUPKEYS,
                                                              null, v -> Stream.of(v.split(",")).map(String::trim).toArray(String[]::new));
        if (configGroupsShortVersion != null) {
            configDtoBuilder.configurations(readSimpleConfigurationRequestEntities(configGroupsShortVersion));
        } else if (properties.containsKey(CONFIGSEEDER_CLIENT_CONFIGURATIONGROUPKEYS + ".0")) {
            configDtoBuilder.configurations(readSimpleConfigurationRequestEntities(properties));
        } else {
            configDtoBuilder.configurations(readConfigurationRequestEntities(properties));
        }

        configBuilder.requestConfiguration(configDtoBuilder.build());

        return configBuilder.build();
    }

    private static List<ConfigurationEntryRequest> readSimpleConfigurationRequestEntities(String... values) {
        return Stream.of(values)
                .map(value -> ConfigurationEntryRequest.builder()
                        .configurationGroupKey(value)
                        .selectionMode(VersionedConfigurationGroupSelectionMode.LATEST)
                        .build())
                .collect(toList());
    }

    private static List<ConfigurationEntryRequest> readSimpleConfigurationRequestEntities(Map<Object, Object> properties) {
        final String groupPrefix = CONFIGSEEDER_CLIENT_CONFIGURATIONGROUPKEYS + ".";
        final String[] configurationGroupKeys = properties.entrySet().stream()
                .filter(e -> String.valueOf(e.getKey()).startsWith(groupPrefix))
                .map(Entry::getValue)
                .distinct()
                .toArray(String[]::new);
        return readSimpleConfigurationRequestEntities(configurationGroupKeys);
    }

    private static List<ConfigurationEntryRequest> readConfigurationRequestEntities(Map<Object, Object> properties) {
        final String groupPrefix = CONFIGSEEDER_CLIENT_CONFIGURATIONS + ".";
        final int startIndex = groupPrefix.length();
        return properties.keySet().stream()
                .filter(e -> String.valueOf(e).startsWith(groupPrefix))
                .map(e -> String.valueOf(e).substring(startIndex))
                .map(e -> e.substring(0, e.indexOf('.') > 0 ? e.indexOf('.') : e.length() - 1)) // NOSONAR
                .distinct()
                .map(groupName -> {
                    final Object keyName = properties.get(groupPrefix + groupName + ".key");
                    final Object selectionMode = properties.get(groupPrefix + groupName + ".selectionMode");
                    final Object version = properties.get(groupPrefix + groupName + ".version");

                    if (keyName == null) {
                        throw new InvalidClientConfigurationException("Missing configuration '" + groupPrefix + groupName + ".key'");
                    }

                    final ConfigurationEntryRequest.ConfigurationEntryRequestBuilder entryBuilder = ConfigurationEntryRequest.builder();
                    entryBuilder.configurationGroupKey(String.valueOf(keyName));
                    if (version != null) {
                        try {
                            entryBuilder.configurationGroupVersionNumber(Integer.valueOf(String.valueOf(version)));
                            entryBuilder.selectionMode(VersionedConfigurationGroupSelectionMode.VERSION_NUMBER);
                        } catch (NumberFormatException e) {
                            throw new InvalidClientConfigurationException("Invalid configuration '" + groupPrefix + groupName + ".version'. Not a number.");
                        }
                    } else if (selectionMode != null) {
                        try {
                            entryBuilder.selectionMode(VersionedConfigurationGroupSelectionMode.valueOf(String.valueOf(selectionMode)));
                        } catch (IllegalArgumentException e) {
                            throw new InvalidClientConfigurationException("Invalid configuration '" + groupPrefix + groupName + ".version'. "
                                                                                  + "Not a valid value of ["
                                                                                  + Stream.of(VersionedConfigurationGroupSelectionMode.values())
                                                                                          .map(Enum::name).collect(joining(","))
                                                                                  + "].");
                        }
                    } else {
                        entryBuilder.selectionMode(VersionedConfigurationGroupSelectionMode.LATEST);
                    }
                    return entryBuilder.build();
                })
                .collect(Collectors.toList());
    }

    private static <V> V fetchConfig(Map<Object, Object> source, String key, V defaultValue, Function<String, V> stringConverter) {
        final Object objectValue = source.get(key);
        String value = null;
        if (objectValue != null) {
            if (objectValue instanceof String) {
                value = (String) objectValue;
            } else if (objectValue instanceof Collection) {
                value = ((Collection<?>) objectValue).stream().map(String::valueOf).collect(joining(","));
            } else if (objectValue.getClass().isArray()) {
                value = Stream.of((Object[]) objectValue).map(String::valueOf).collect(joining(","));
            } else {
                value = String.valueOf(objectValue);
            }
        }
        final String parameterOrEnvironmentValue = ParameterUtil.getParameter(key);
        if (parameterOrEnvironmentValue != null) {
            value = parameterOrEnvironmentValue;
        }
        if (value != null) {
            return stringConverter.apply(value);
        } else {
            return defaultValue;
        }
    }

    private static String fetchConfig(Map<Object, Object> source, String key) {
        return fetchConfig(source, key, null, identity());
    }

    private static String fetchConfig(Map<Object, Object> source, String key, String defaultValue) {
        return fetchConfig(source, key, defaultValue, String::valueOf);
    }

    private static Integer fetchIntConfig(Map<Object, Object> source, String key, int defaultValue) {
        return fetchConfig(source, key, defaultValue, Integer::valueOf);
    }

}
