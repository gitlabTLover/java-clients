/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client.util;

import lombok.experimental.UtilityClass;

/**
 * Helps reading values from the system environment or Parameter.
 */
@UtilityClass
public class ParameterUtil {

    /**
     * Reads a parameter from system property and if not present from environment.
     *
     * @param key configuration key
     * @return configuration value if found in one of the system or env properties
     */
    public static String getParameter(String key) {
        // system property
        final String systemParameter = System.getProperty(key);
        if (systemParameter != null) {
            return systemParameter.trim();
        }

        // environment
        final String environmentParameter = System.getenv(snakeCase(key));
        if (environmentParameter != null) {
            return environmentParameter.trim();
        }

        return null;
    }

    static String snakeCase(String key) {
        return key
                .replace("-", "")
                .replace('.', '_')
                .toUpperCase();
    }

}
