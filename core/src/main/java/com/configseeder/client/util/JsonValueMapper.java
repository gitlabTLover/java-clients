/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client.util;

import com.configseeder.client.Logger;
import com.configseeder.client.model.ConfigValue;
import com.configseeder.client.model.ConfigurationValueType;
import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class JsonValueMapper {

    private static final Logger logger = Logger.getLogger(JsonValueMapper.class);

    /**
     * Writes a collection of configuration values to a string.
     *
     * @param values configuration values
     * @return serialized as json string
     */
    public String write(Collection<ConfigValue> values) {
        JsonArray jsonArray = new JsonArray();
        if (values != null) {
            values.forEach(value -> jsonArray.add(mapConfigValue(value)));
        }

        Writer writer = new StringWriter();
        try {
            jsonArray.writeTo(writer);
        } catch (IOException e) {
            logger.warn("Unable to serialize config values to string.", e);
        }

        return writer.toString();
    }

    /**
     * Reads a json string which is expected to deliver config values.
     *
     * @param content json
     * @return config values map by key -> object
     */
    public Map<String, ConfigValue> parse(String content) {
        final JsonValue jsonValue = Json.parse(content);
        if (!jsonValue.isArray()) {
            return Collections.emptyMap();
        }

        final JsonArray rootObject = jsonValue.asArray();
        final Map<String, ConfigValue> values = new LinkedHashMap<>();
        for (JsonValue jsonArrayValue : rootObject.values()) {
            final JsonObject jsonObject = jsonArrayValue.asObject();
            final ConfigValue configValue = mapConfigValue(jsonObject);
            values.put(configValue.getKey(), configValue);
        }
        return values;
    }

    ConfigValue mapConfigValue(JsonObject jsonObject) {
        final ConfigValue configValue = new ConfigValue();

        JsonValue key = jsonObject.get("key");
        configValue.setKey(key == null || key.isNull() ? null : key.asString());

        JsonValue value = jsonObject.get("value");
        configValue.setValue(value == null || value.isNull()
                             ? null
                             : value.asString());

        JsonValue type = jsonObject.get("type");
        configValue.setType(type == null || type.isNull()
                            ? ConfigurationValueType.STRING
                            : ConfigurationValueType.safeValueOf(type.asString(), ConfigurationValueType.STRING));

        JsonValue lastUpdateInMilliseconds = jsonObject.get("lastUpdateInMilliseconds");
        configValue.setLastUpdateInMilliseconds(lastUpdateInMilliseconds == null || lastUpdateInMilliseconds.isNull()
                                                ? System.currentTimeMillis()
                                                : lastUpdateInMilliseconds.asLong());

        JsonValue lastUpdate = jsonObject.get("lastUpdate");
        configValue.setLastZonedUpdate(lastUpdate == null || lastUpdate.isNull()
                                       ? ZonedDateTime.now()
                                       : mapZonedDateTime(lastUpdate.asString()));

        return configValue;
    }

    private JsonObject mapConfigValue(ConfigValue configValue) {
        JsonObject jsonObject = Json.object();
        jsonObject.add("key", configValue.getKey());
        jsonObject.add("value", configValue.getValue());
        jsonObject.add("type", configValue.getType().name());
        jsonObject.add("lastUpdateInMilliseconds", configValue.getLastUpdateInMilliseconds());
        jsonObject.add("lastUpdate", mapZonedDateTime(configValue.getLastZonedUpdate()));
        return jsonObject;
    }

    ZonedDateTime mapZonedDateTime(String lastUpdate) {
        if (lastUpdate == null) {
            return null;
        }
        try {
            return ZonedDateTime.parse(lastUpdate);
        } catch (DateTimeParseException e) {
            return ZonedDateTime.of(LocalDateTime.parse(lastUpdate), ZoneId.systemDefault());
        }
    }

    String mapZonedDateTime(ZonedDateTime zonedDateTime) {
        if (zonedDateTime == null) {
            return null;
        } else {
            return DateTimeFormatter.ISO_ZONED_DATE_TIME.format(zonedDateTime);
        }
    }

}
