/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client;

import static java.util.Collections.emptyMap;
import static java.util.Collections.unmodifiableMap;
import static java.util.Collections.unmodifiableSet;
import static java.util.stream.Collectors.toMap;

import com.configseeder.client.model.ConfigValue;
import com.configseeder.client.model.ConfigurationEntryRequest;
import com.configseeder.client.model.ConfigurationRequest;
import com.configseeder.client.model.ConfigurationValueType;
import com.configseeder.client.model.Identification;
import com.configseeder.client.util.JsonValueMapper;
import com.configseeder.client.util.RecoveryFileConfigurationRepository;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * Main entry point to get configurations or properties.
 */
@Getter
public class ConfigSeederClient {

    /** Logger. */
    private final Logger logger = new Logger(ConfigSeederClient.class);

    /** refresh strategy. */
    @Setter
    private RefreshMode refreshMode = RefreshMode.MANUAL;

    /** initialization count down latch. */
    private CountDownLatch refreshLatch = new CountDownLatch(1);

    /** checks if is initialized. */
    private boolean initialized = false;

    /** refresh properties every 60 seconds. */
    @Setter
    private int refresh = 60000;

    /** maximum retries. */
    @Setter
    private int maxRetries = 2;

    /** retry wait time in milliseconds. */
    @Setter
    private int retryWaitTime = 2000;

    /** Config Rest Client. */
    private ConfigSeederRestClient configSeederRestClient;

    /** At which time last synchronization took place. */
    private long lastSuccessfulSynchronization = 0;

    /** At which time last synchronization was started. */
    private long lastFetchExecution = 0;

    /** Configuration request. */
    private final ConfigurationRequest configurationRequest;

    /** All properties. */
    @Getter(AccessLevel.NONE)
    private Map<String, ConfigValue> values = emptyMap();

    /** Reload execution timer. */
    private Timer timer;

    /** Executor to asynchronously fetch values. */
    private final ExecutorService executor = Executors.newFixedThreadPool(1);

    /** Contains all listeners which should be notified if a key was updated. */
    private final Set<KeyUpdateListener> keyUpdateListeners = new HashSet<>();

    private RecoveryFileConfigurationRepository repository;

    /**
     * C'tor.
     *
     * @param configSeederClientConfiguration client configuration
     * @param identification identification
     */
    public ConfigSeederClient(ConfigSeederClientConfiguration configSeederClientConfiguration, Identification identification) {
        this(configSeederClientConfiguration, new ConfigSeederRestClient(
                configSeederClientConfiguration.getServerUrl(),
                configSeederClientConfiguration.getApiKey(),
                configSeederClientConfiguration.getConnectionTimeout(),
                configSeederClientConfiguration.getReadTimeout(),
                identification));
        repository = new RecoveryFileConfigurationRepository(new JsonValueMapper());
        repository.setRecoveryFilePath(configSeederClientConfiguration.getRecoveryFile());

        logger.info(configSeederClientConfiguration.toString());
    }

    /**
     * C'tor.
     *
     * @param configSeederClientConfiguration configuration
     * @param restClient rest client
     */
    public ConfigSeederClient(ConfigSeederClientConfiguration configSeederClientConfiguration, ConfigSeederRestClient restClient) {
        configSeederClientConfiguration.validate();
        this.repository = new RecoveryFileConfigurationRepository(new JsonValueMapper());
        this.repository.setRecoveryFilePath(configSeederClientConfiguration.getRecoveryFile());

        this.configurationRequest = configSeederClientConfiguration.getRequestConfiguration();
        this.refreshLatch = new CountDownLatch(0);

        if (configSeederClientConfiguration.getRefreshCycle() != null && configSeederClientConfiguration.getRefreshCycle() >= 100) {
            this.refresh = configSeederClientConfiguration.getRefreshCycle();
        }
        if (configSeederClientConfiguration.getMaxRetries() != null && configSeederClientConfiguration.getMaxRetries() >= 0
                && configSeederClientConfiguration.getMaxRetries() < 10) {
            this.maxRetries = configSeederClientConfiguration.getMaxRetries();
        }
        if (configSeederClientConfiguration.getRetryWaitTime() != null && configSeederClientConfiguration.getRetryWaitTime() >= 0
                && configSeederClientConfiguration.getRetryWaitTime() <= 120000) {
            this.retryWaitTime = configSeederClientConfiguration.getRetryWaitTime();
        }

        if (configSeederClientConfiguration.isDisabled()) {
            this.refreshMode = RefreshMode.NEVER;
        } else {
            this.configSeederRestClient = restClient;
            this.refreshMode = configSeederClientConfiguration.getRefreshMode();
            switch (configSeederClientConfiguration.getInitializationMode()) {
                case EAGER:
                    initializeValues();
                    break;
                case EAGER_ASYNC:
                    executor.submit(this::initializeValues);
                    break;
                case LAZY:
                    break;
                default:
                    throw new IllegalArgumentException("Initialization mode '" + configSeederClientConfiguration.getInitializationMode() + "' not known");
            }
        }
        if (RefreshMode.TIMER.equals(refreshMode)) {
            setupTimer();
        }
    }

    private synchronized void setupTimer() {
        if (timer != null) {
            timer.cancel();
        }
        timer = new Timer("config-reload-timer", true);
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                refreshValues();
            }
        }, refresh, refresh);
    }

    /**
     * Refresh values. Will only throw exceptions if after certain retries server is not reachable.
     *
     * @return {@code true} if values have been refreshed. {@code false} if process was already started.
     */
    private boolean refreshValues() {
        if (RefreshMode.NEVER.equals(refreshMode)) {
            logger.debug("Refresh prevented. No refresh");
            return false;
        }
        if (refreshLatch.getCount() >= 1) {
            logger.debug("Fetch already in progress. Ignored.");
            return false;
        }
        refreshLatch = new CountDownLatch(1);
        lastFetchExecution = System.currentTimeMillis();

        try {
            final long start = System.currentTimeMillis();
            final String requestUrl = configSeederRestClient.getRequestUrl();
            logger.debug("Start fetching values from '{}'", requestUrl);
            int tryCount = 1;
            while (tryCount <= maxRetries + 1) {
                try {
                    Map<String, ConfigValue> oldValues = values;
                    values = configSeederRestClient.getProperties(configurationRequest);
                    repository.saveConfigValues(values.values());
                    notifyUpdates(oldValues, values);
                    lastSuccessfulSynchronization = System.currentTimeMillis();
                    logger.debug("Successfully retrieved '{}' values within '{}' msec and '{}' tries",
                                 values.size(), (lastSuccessfulSynchronization - start), tryCount);
                    return true;
                } catch (RequestTimedOutException e) {
                    logger.warn("Could not fetch values from '" + requestUrl + "' (try: {}).", tryCount, e);
                } catch (ConfigurationValueServerHttpStatusException statusException) {
                    final String urlCouldNotBeFetched = "URL '" + requestUrl + "' could not be fetched. ";
                    if (statusException.getStatusCode() >= 500) {
                        logger.warn(urlCouldNotBeFetched + "'. Got error '" + statusException.getStatusCode() + "'", statusException);
                        return false;
                    } else if (statusException.getStatusCode() == 401) {
                        throw new InvalidClientConfigurationException(urlCouldNotBeFetched + "Not authorized.", statusException);
                    } else if (statusException.getStatusCode() == 403) {
                        throw new InvalidClientConfigurationException(urlCouldNotBeFetched + "Invalid credentials.", statusException);
                    } else if (statusException.getStatusCode() == 404) {
                        throw new InvalidClientConfigurationException(urlCouldNotBeFetched + "URL not served.", statusException);
                    } else if (statusException.getStatusCode() == 400) {
                        throw new InvalidClientConfigurationException(urlCouldNotBeFetched + "Invalid parameters: "
                                                                              + statusException.getMessage(), statusException);
                    } else if (statusException.getStatusCode() >= 300) {
                        throw new InvalidClientConfigurationException(urlCouldNotBeFetched + "Invalid state: "
                                                                              + statusException.getStatusCode() + ": " + statusException.getMessage(),
                                                                      statusException);
                    }
                    logger.error("Got status code '" + statusException.getStatusCode() + "' which have not be handled");
                    throw new IllegalStateException("Not supported status code '" + statusException.getStatusCode() + "'", statusException);
                }
                tryCount++;
                if (tryCount <= maxRetries + 1) {
                    try {
                        Thread.sleep(retryWaitTime);
                    } catch (InterruptedException e) {
                        logger.warn("Thread interrupted. Retry fetching values");
                        Thread.currentThread().interrupt();
                    }
                }
            }
            logger.info("Could not fetch values from '{}' within '{}' tries with wait time '{}'", requestUrl, maxRetries + 1, retryWaitTime);
            return false;
        } finally {
            refreshLatch.countDown();
        }
    }

    void notifyUpdates(Map<String, ConfigValue> oldValues, Map<String, ConfigValue> values) {
        final Map<String, KeyUpdate> updates = new HashMap<>();
        Stream.concat(oldValues.keySet().stream(), values.keySet().stream()).distinct().forEach(key -> {
            final ConfigValue oldValue = oldValues.get(key);
            final ConfigValue newValue = values.get(key);
            boolean isNew = oldValue == null;
            boolean isDeleted = newValue == null;
            if (isNew || isDeleted || oldValue.getLastUpdateInMilliseconds() != newValue.getLastUpdateInMilliseconds()) {
                updates.put(key, new KeyUpdate(key, oldValue, newValue, isNew, isDeleted));
            }
        });
        if (updates.size() > 0) {
            logger.info("New values received for keys: '{}'", String.join("','", updates.keySet()));
            final Map<String, KeyUpdate> updatedKeysUnmodifiable = unmodifiableMap(updates);
            keyUpdateListeners.forEach(listener -> listener.onUpdatedKeys(updatedKeysUnmodifiable));
        }
    }

    /**
     * Adds the specified listener. If already contained it will return false.
     *
     * @param listener the listener to subscribe
     * @return true if not yet contained and added
     */
    public boolean subscribe(KeyUpdateListener listener) {
        return keyUpdateListeners.add(listener);
    }

    /**
     * Unsubscribes a listener. If contained, returns true.
     *
     * @param listener the listener to unsubscribe
     * @return true if contained and removed
     */
    public boolean unsubscribe(KeyUpdateListener listener) {
        return keyUpdateListeners.remove(listener);
    }

    /**
     * Shutdown refresh timer.
     */
    public void shutdown() {
        this.refreshMode = RefreshMode.NEVER;
        if (this.timer != null) {
            this.timer.cancel();
        }
    }

    private Map<String, ConfigValue> getValueMap() {
        // Lazy initialization or failed to initialize values
        initializeValues();

        // Reload values if needed (only needed in lazy mode)
        checkAndReload();

        // Now values should be fine to be retrieved
        return values;
    }

    private void initializeValues() {
        // Is already initialized?
        if (!initialized) {
            if (refreshValues()) {
                initialized = true;
            } else if (repository.getRecoveryFilePath() != null) {
                logger.info("Initialization completed using persistency fallback: " + repository.getRecoveryFilePath());
                values = repository.getPersistedValues();
                initialized = true;
            }

            // Check if initialization is completed. If not, wait. This situation should be quite rare.
            try {
                refreshLatch.await();
            } catch (InterruptedException e) {
                logger.warn("Thread interrupted. Maybe values are not yet ready.");
                Thread.currentThread().interrupt();
            }
        }
    }

    private void checkAndReload() {
        if (isRefreshNeeded()) {
            logger.debug("Refresh needed");
            if (RefreshMode.LAZY.equals(refreshMode)) {
                blockingReload();
            } else if (RefreshMode.TIMER.equals(refreshMode)) {
                checkTimedReload();
            }
        }
    }

    private void blockingReload() {
        if (!refreshValues()) {
            try {
                refreshLatch.await();
            } catch (InterruptedException e) {
                logger.warn("Thread interrupted.");
                Thread.currentThread().interrupt();
            }
        }
    }

    private void checkTimedReload() {
        if (lastFetchExecution + refresh + refresh < System.currentTimeMillis()) {
            if (refreshLatch.getCount() > 0) {
                logger.debug("Refresh in progress, will use old data.");
            } else {
                logger.warn("Timer seems to be dead. Restore");
                setupTimer();
            }
        } else {
            logger.debug("Refresh executed, but no successful update received. Will still use old data.");
        }
    }

    private boolean isRefreshNeeded() {
        // It's ok to refresh 100 milliseconds earlier
        return lastSuccessfulSynchronization + refresh - 100 < System.currentTimeMillis();
    }

    /**
     * Get value as string.
     *
     * @param key key
     * @return the value
     */
    public String getString(String key) {
        return provideValue(key, (value, valueType) -> {
            if (valueType == ConfigurationValueType.BINARY) {
                throw new ConfigSeederTechnicalException("Cannot convert type '" + valueType + "' to a string for value '" + value + "'");
            }
            return value;
        });
    }

    /**
     * Gets configuration value as integer.
     *
     * @param key key
     * @return the value
     */
    public Integer getInteger(String key) {
        return provideValue(key, (value, valueType) -> {
            switch (valueType) {
                case INTEGER:
                    return Integer.parseInt(value);
                case NUMBER:
                case REGEX:
                case STRING:
                case MULTILINE_STRING:
                case ENUM:
                case API_KEY_TYPE:
                    try {
                        return Integer.parseInt(value);
                    } catch (NumberFormatException e) {
                        throw new ConfigSeederTechnicalException(
                                "Called unsafe getInteger() for value '" + value + "' of type '" + valueType + "' to an integer", e);
                    }
                case BOOLEAN:
                    return Boolean.parseBoolean(value) ? 1 : 0;
                case BINARY:
                default:
                    throw new ConfigSeederTechnicalException("Cannot convert type '" + valueType + "' to an integer for value '" + value + "'");
            }
        });
    }

    /**
     * Gets configuration value as integer.
     *
     * @param key key
     * @return the value
     */
    public Boolean getBoolean(String key) {
        return provideValue(key, (value, valueType) -> {
            switch (valueType) {
                case BOOLEAN:
                    return Boolean.valueOf(value);
                case INTEGER:
                    final int intValue = Integer.parseInt(value);
                    if (intValue == 1) {
                        return true;
                    } else if (intValue == 0) {
                        return false;
                    } else {
                        throw new ConfigSeederTechnicalException("Value '" + intValue + "' is not a valid boolean");
                    }
                default:
                    throw new ConfigSeederTechnicalException("Cannot convert type '" + valueType + "' to an integer for value '" + value + "'");
            }
        });
    }

    private <T> T provideValue(String key, BiFunction<String, ConfigurationValueType, T> converter) {
        final ConfigValue configValueDto = getValueMap().get(key);
        if (configValueDto == null || configValueDto.getValue() == null) {
            return null;
        }
        return converter.apply(configValueDto.getValue(), configValueDto.getType());
    }

    /**
     * Lists all available keys.
     *
     * @return all keys
     */
    public Set<String> getKeys() {
        return unmodifiableSet(getValueMap().keySet());
    }

    /**
     * Lists all values.
     *
     * @return all values
     */
    public Map<String, String> getProperties() {
        return getValueMap().entrySet().stream().collect(toMap(Map.Entry::getKey, e -> e.getValue().getValue()));
    }

    /**
     * Retrieves all values.
     *
     * @return all values to be retrieved
     */
    public Map<String, ConfigValue> getValues() {
        return unmodifiableMap(getValueMap());
    }

    /**
     * Forced refresh of the values.
     */
    public void forcedRefresh() {
        refreshValues();
    }

    /**
     * Name of this configuration provider.
     *
     * @return name of this configuration
     */
    public String getName() {
        return this.getConfigSeederRestClient().getRequestUrl() + ":"
                + getConfigurationRequest().getConfigurations().stream().map(ConfigurationEntryRequest::getConfigurationGroupKey)
                .collect(Collectors.joining(","))
                + ":" + getConfigurationRequest().getEnvironmentKey();
    }

}
