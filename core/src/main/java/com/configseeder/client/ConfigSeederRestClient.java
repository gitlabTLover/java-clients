/*
 * Copyright (c) 2021 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client;

import com.configseeder.client.model.ConfigValue;
import com.configseeder.client.model.ConfigurationEntryRequest;
import com.configseeder.client.model.ConfigurationRequest;
import com.configseeder.client.model.ConfigurationValueType;
import com.configseeder.client.model.Identification;
import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.charset.UnsupportedCharsetException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import javax.annotation.Nonnull;
import lombok.Getter;
import lombok.Setter;

/**
 * Rest client able to retrieve values from ConfigSeeder.
 */
@Getter
@Setter
class ConfigSeederRestClient {

    static final String HEADER_ACCEPT = "Accept";
    static final String HEADER_CONTENT_TYPE = "Content-Type";
    static final String HEADER_AUTHORIZATION = "Authorization";

    @SuppressWarnings("squid:S1075") // fixed path is ok here
    static final String PATH_TO_EXTERNAL_PROPERTIES = "/public/api/v1/configurations";
    static final String APPLICATION_JSON_METADATA = "application/metadata+json";

    /** Logger. */
    private final Logger logger = new Logger(ConfigSeederRestClient.class);

    private final String url;
    private int connectionTimeout;
    private int readTimeout;
    private Identification identification;
    private final String apiKey;


    ConfigSeederRestClient(String url, String apiKey) {
        this(url, apiKey, 500, 2000, Identification.create("Core"));
    }

    ConfigSeederRestClient(@Nonnull String url, @Nonnull String apiKey,
                           int connectionTimeout, int readTimeout, @Nonnull Identification identification) {
        this.connectionTimeout = connectionTimeout;
        this.readTimeout = readTimeout;
        this.identification = identification;
        if (!url.startsWith("http")) {
            throw new IllegalArgumentException("URL '" + url + "' seems not be a valid url");
        }
        if (url.endsWith("/")) {
            this.url = url.substring(0, url.length() - 1);
        } else {
            this.url = url;
        }
        if (apiKey.trim().length() == 0) {
            throw new IllegalArgumentException("API Key should not be null");
        }
        this.apiKey = apiKey;
    }

    /**
     * Fetch properties.
     *
     * @param configurationRequest configuration request
     * @return loaded properties
     *
     * @throws RequestTimedOutException on request time out
     * @throws ConfigSeederTechnicalException other technical errors (retry needed)
     * @throws ConfigurationValueServerHttpStatusException any other status code errors
     */
    Map<String, ConfigValue> getProperties(ConfigurationRequest configurationRequest)
            throws RequestTimedOutException, ConfigurationValueServerHttpStatusException {
        configurationRequest.validate();
        final String requestUrl = getRequestUrl();
        final String requestBody = buildRequestBody(configurationRequest);
        logger.debug("Get properties from '{}'", requestUrl);
        try {
            final URL siteUrl = new URL(requestUrl);
            final HttpURLConnection httpConnection = (HttpURLConnection) siteUrl.openConnection();
            httpConnection.setRequestMethod("POST");
            // time to connect
            httpConnection.setConnectTimeout(connectionTimeout);
            // time to response
            httpConnection.setReadTimeout(readTimeout);
            httpConnection.setDefaultUseCaches(false);
            httpConnection.setRequestProperty(HEADER_ACCEPT, APPLICATION_JSON_METADATA);
            httpConnection.setRequestProperty(HEADER_CONTENT_TYPE, APPLICATION_JSON_METADATA + "; charset=UTF-8");
            httpConnection.setRequestProperty(HEADER_AUTHORIZATION, "Bearer " + apiKey);
            httpConnection.setRequestProperty(Identification.HEADER_X_USER_AGENT, identification.getUserAgent());
            httpConnection.setRequestProperty(Identification.HEADER_X_HOSTNAME, identification.getHostname());
            httpConnection.setRequestProperty(Identification.HEADER_X_HOST_IDENTIFICATION, identification.getHostIdentification());
            httpConnection.setDoOutput(true);
            httpConnection.setDoInput(true);
            httpConnection.connect();
            try (OutputStreamWriter streamWriter = new OutputStreamWriter(httpConnection.getOutputStream())) {
                streamWriter.write(requestBody);
                streamWriter.flush();
                if (httpConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    final String responseMessage = httpConnection.getResponseMessage();
                    String errorStreamMessage = null;
                    if (httpConnection.getErrorStream() != null) {
                        errorStreamMessage = new Scanner(httpConnection.getErrorStream(), "UTF-8").useDelimiter("\\A").next();
                    }
                    String msg = String.format("Failed to load configurations from '%s' with request object%n%s%nError %d: %s%nError Msg: %s",
                                               requestUrl, requestBody, httpConnection.getResponseCode(), responseMessage, errorStreamMessage);
                    logger.info(msg);
                    throw new ConfigurationValueServerHttpStatusException(httpConnection.getResponseCode(), errorStreamMessage != null ? errorStreamMessage :
                                                                                                            responseMessage);
                }

                final String content = new Scanner(httpConnection.getInputStream(), "UTF-8").useDelimiter("\\A").next();
                final Map<String, ConfigValue> values = parseConfigValues(content);

                httpConnection.disconnect();

                logger.debug("Get successfully '{}' configurations", values.size());
                return values;
            }
        } catch (UnsupportedCharsetException e) {
            throw new ConfigSeederTechnicalException("Could not decode to UTF-8", e);
        } catch (MalformedURLException e) {
            throw new InvalidClientConfigurationException("URL '" + requestUrl + "' is invalid", e);
        } catch (SocketTimeoutException e) {
            throw new RequestTimedOutException("Could not connect to " + requestUrl + " within " + connectionTimeout + "msec.", e);
        } catch (IOException e) {
            throw new ConfigSeederTechnicalException("Could not connect and fetch values", e);
        }

    }

    private Map<String, ConfigValue> parseConfigValues(String content) {
        final JsonValue jsonValue = Json.parse(content);
        final JsonArray rootObject = jsonValue.asArray();
        final Map<String, ConfigValue> values = new LinkedHashMap<>();
        for (JsonValue jsonArrayValue : rootObject.values()) {
            final JsonObject jsonObject = jsonArrayValue.asObject();
            final ConfigValue configValue = new ConfigValue();
            final JsonValue value = jsonObject.get("value");
            configValue.setValue(value.isNull() ? null : value.asString());
            configValue.setType(ConfigurationValueType.valueOf(jsonObject.get("type").asString()));
            final JsonValue lastUpdateInMilliseconds = jsonObject.get("lastUpdateInMilliseconds");
            configValue.setLastUpdateInMilliseconds(lastUpdateInMilliseconds == null || lastUpdateInMilliseconds.isNull()
                                                    ? System.currentTimeMillis()
                                                    : lastUpdateInMilliseconds.asLong());
            final JsonValue lastUpdate = jsonObject.get("lastUpdate");
            configValue.setLastZonedUpdate(lastUpdate == null || lastUpdate.isNull()
                                           ? ZonedDateTime.now()
                                           : parseDateTime(lastUpdate.asString()));
            configValue.setKey(jsonObject.get("key").asString());
            values.put(configValue.getKey(), configValue);
        }
        return values;
    }

    private static ZonedDateTime parseDateTime(String lastUpdate) {
        try {
            return ZonedDateTime.parse(lastUpdate);
        } catch (DateTimeParseException e) {
            return ZonedDateTime.of(LocalDateTime.parse(lastUpdate), ZoneId.systemDefault());
        }
    }

    String getRequestUrl() {
        return String.format("%s%s", url, PATH_TO_EXTERNAL_PROPERTIES);
    }

    private String buildRequestBody(ConfigurationRequest configurationRequest) {
        final JsonObject object = Json.object();
        object.add("tenantKey", String.valueOf(configurationRequest.getTenantKey()));
        object.add("configurations", toJsonValue(configurationRequest.getConfigurations()));
        if (configurationRequest.getEnvironmentKey() != null) {
            object.add("environmentKey", configurationRequest.getEnvironmentKey());
        }
        if (configurationRequest.getVersion() != null) {
            object.add("version", configurationRequest.getVersion());
        }
        if (configurationRequest.getContext() != null) {
            object.add("context", configurationRequest.getContext());
        }
        if (configurationRequest.getDateTime() != null) {
            object.add("dateTime", String.valueOf(configurationRequest.getDateTime()));
        } else {
            object.add("dateTime", String.valueOf(LocalDateTime.now()));
        }
        return object.toString();
    }

    private JsonArray toJsonValue(List<ConfigurationEntryRequest> configurations) {
        final JsonArray jsonArray = new JsonArray();
        configurations.forEach(config -> {
            final JsonObject configEntry = Json.object();
            if (config.getVersionedConfigurationGroupId() != null) {
                configEntry.add("versionedConfigurationGroupId", config.getVersionedConfigurationGroupId());
            }
            if (config.getConfigurationGroupId() != null) {
                configEntry.add("configurationGroupId", config.getConfigurationGroupId());
            }
            if (config.getConfigurationGroupKey() != null) {
                configEntry.add("configurationGroupKey", config.getConfigurationGroupKey());
            }
            if (config.getConfigurationGroupVersionNumber() != null) {
                configEntry.add("configurationGroupVersionNumber", config.getConfigurationGroupVersionNumber());
            }
            if (config.getSelectionMode() != null) {
                configEntry.add("selectionMode", String.valueOf(config.getSelectionMode()));
            }
            jsonArray.add(configEntry);
        });
        return jsonArray;
    }

}
