/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

import com.configseeder.client.model.ConfigValue;
import com.configseeder.client.model.ConfigurationEntryRequest;
import com.configseeder.client.model.ConfigurationRequest;
import com.configseeder.client.model.ConfigurationValueType;
import com.configseeder.client.model.VersionedConfigurationGroupSelectionMode;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import org.assertj.core.util.Maps;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ConfigSeederClientTest {

    private static final ZonedDateTime PREVIOUS = ZonedDateTime.now().minusDays(1);

    @Mock
    private ConfigSeederClientConfiguration clientConfiguration;
    @Mock
    private ConfigSeederRestClient configSeederRestClient;

    private ConfigSeederClient configSeederClient;

    @Test
    void shouldDeserializeAllToString() throws RequestTimedOutException, ConfigurationValueServerHttpStatusException {
        // given
        final Map<String, ConfigValue> valueMap = new HashMap<>();
        valueMap.put(ConfigurationValueType.STRING.name(), cv(ConfigurationValueType.STRING, "simple-text"));
        valueMap.put(ConfigurationValueType.MULTILINE_STRING.name(), cv(ConfigurationValueType.MULTILINE_STRING, "simple\ntext"));
        valueMap.put(ConfigurationValueType.BOOLEAN.name(), cv(ConfigurationValueType.BOOLEAN, "true"));
        valueMap.put(ConfigurationValueType.INTEGER.name(), cv(ConfigurationValueType.INTEGER, "-5"));
        valueMap.put(ConfigurationValueType.NUMBER.name(), cv(ConfigurationValueType.NUMBER, "-42.344"));
        valueMap.put(ConfigurationValueType.REGEX.name(), cv(ConfigurationValueType.REGEX, "email@server.com"));
        valueMap.put(ConfigurationValueType.CONFIGURATION_GROUP.name(), cv(ConfigurationValueType.CONFIGURATION_GROUP, "80e10401-cfa8-4943-8154-b138e3c1aa52"));
        valueMap.put(ConfigurationValueType.API_KEY_TYPE.name(), cv(ConfigurationValueType.API_KEY_TYPE, "CLIENT"));
        valueMap.put(ConfigurationValueType.ENUM.name(), cv(ConfigurationValueType.ENUM, "INFO"));
        valueMap.put(ConfigurationValueType.CERTIFICATE.name(), cv(ConfigurationValueType.CERTIFICATE, "-----BEGIN CERTIFICATE-----\n"
                + "MIICWwIBAAKBgQCzURxIqzer0ACAbX/lHdsn4Gd9PLKrf7EeDYfIdV0HZKPD8WDr"));
        valueMap.put(ConfigurationValueType.PRIVATE_KEY.name(), cv(ConfigurationValueType.PRIVATE_KEY, "-----BEGIN RSA PRIVATE KEY-----\n"
                + "MIICWwIBAAKBgQCzURxIqzer0ACAbX/lHdsn4Gd9PLKrf7EeDYfIdV0HZKPD8WDr"));
        when(configSeederRestClient.getProperties(any(ConfigurationRequest.class))).thenReturn(valueMap);
        final ConfigSeederClientConfiguration clientConfig = buildClientConfig();
        configSeederClient = new ConfigSeederClient(clientConfig, configSeederRestClient);

        for (ConfigurationValueType valueType : ConfigurationValueType.values()) {
            if (ConfigurationValueType.BINARY == valueType) {
                continue;
            }

            // when
            final String value = configSeederClient.getString(valueType.name());

            // then
            assertThat(value).as(valueType.name()).isNotBlank();
        }
    }

    @Test
    void shouldDeserializeToInteger() throws RequestTimedOutException, ConfigurationValueServerHttpStatusException {
        // given
        final Map<String, ConfigValue> valueMap = new HashMap<>();
        valueMap.put(ConfigurationValueType.STRING.name(), cv(ConfigurationValueType.STRING, "42"));
        valueMap.put(ConfigurationValueType.MULTILINE_STRING.name(), cv(ConfigurationValueType.MULTILINE_STRING, "-123"));
        valueMap.put(ConfigurationValueType.BOOLEAN.name(), cv(ConfigurationValueType.BOOLEAN, "true"));
        valueMap.put(ConfigurationValueType.INTEGER.name(), cv(ConfigurationValueType.INTEGER, "-5"));
        valueMap.put(ConfigurationValueType.NUMBER.name(), cv(ConfigurationValueType.NUMBER, "-42"));
        valueMap.put(ConfigurationValueType.REGEX.name(), cv(ConfigurationValueType.REGEX, "121"));
        valueMap.put(ConfigurationValueType.ENUM.name(), cv(ConfigurationValueType.ENUM, "0"));

        valueMap.put(ConfigurationValueType.CONFIGURATION_GROUP.name(), cv(ConfigurationValueType.CONFIGURATION_GROUP, "80e10401-cfa8-4943-8154-b138e3c1aa52"));
        valueMap.put(ConfigurationValueType.API_KEY_TYPE.name(), cv(ConfigurationValueType.API_KEY_TYPE, "CLIENT"));

        when(configSeederRestClient.getProperties(any(ConfigurationRequest.class))).thenReturn(valueMap);
        final ConfigSeederClientConfiguration clientConfig = buildClientConfig();
        configSeederClient = new ConfigSeederClient(clientConfig, configSeederRestClient);

        assertThat(configSeederClient.getInteger(ConfigurationValueType.STRING.name())).isEqualTo(42);
        assertThat(configSeederClient.getInteger(ConfigurationValueType.MULTILINE_STRING.name())).isEqualTo(-123);
        assertThat(configSeederClient.getInteger(ConfigurationValueType.BOOLEAN.name())).isEqualTo(1);
        assertThat(configSeederClient.getInteger(ConfigurationValueType.INTEGER.name())).isEqualTo(-5);
        assertThat(configSeederClient.getInteger(ConfigurationValueType.NUMBER.name())).isEqualTo(-42);
        assertThat(configSeederClient.getInteger(ConfigurationValueType.REGEX.name())).isEqualTo(121);
        assertThat(configSeederClient.getInteger(ConfigurationValueType.ENUM.name())).isEqualTo(0);

        assertThrows(ConfigSeederTechnicalException.class, () -> configSeederClient.getInteger(ConfigurationValueType.CONFIGURATION_GROUP.name()));
        assertThrows(ConfigSeederTechnicalException.class, () -> configSeederClient.getInteger(ConfigurationValueType.API_KEY_TYPE.name()));
    }

    @Test
    void shouldNotKillTimerWhenStillRunning()
            throws InterruptedException, ExecutionException, RequestTimedOutException, ConfigurationValueServerHttpStatusException {
        // given
        final AtomicInteger accessCounter = new AtomicInteger(0);
        final ConfigSeederClientConfiguration clientConfig = ConfigSeederClientConfiguration.builder()
                .serverUrl("http://localhost/").apiKey("just-some-api-key")
                .initializationMode(InitializationMode.LAZY).refreshMode(RefreshMode.TIMER)
                .maxRetries(0)
                .refreshCycle(100)
                .requestConfiguration(ConfigurationRequest.builder()
                                              .configurations(singletonList(ConfigurationEntryRequest.builder().configurationGroupKey("CG")
                                                                                    .selectionMode(VersionedConfigurationGroupSelectionMode.LATEST)
                                                                                    .build())).build())
                .build();
        configSeederClient = new ConfigSeederClient(clientConfig, configSeederRestClient);
        configSeederClient.getInteger("intitial.value");

        given(configSeederRestClient.getProperties(any(ConfigurationRequest.class))).willAnswer(a -> {
            int accessIndex = accessCounter.getAndIncrement();
            if (accessIndex == 0) {
                return Maps.newHashMap("initial.value", cv(ConfigurationValueType.INTEGER, "0"));
            } else if (accessIndex == 1) {
                throw new RequestTimedOutException("simulate a timeout");
            } else {
                return Maps.newHashMap("string.value", cv(ConfigurationValueType.STRING, "val2"));
            }
        });

        ExecutorService executorService = Executors.newFixedThreadPool(2);

        // when
        Thread.sleep(110); // NOSONAR  : sleep more than refresh cycle. next request should actually trigger a refresh.
        Future<String> firstResult = executorService.submit(() -> configSeederClient.getString("string.value"));

        // then
        assertThat(firstResult.get()).isNull();

        // when
        Thread.sleep(210); // NOSONAR : sleep more than refresh cycle. next request should actually trigger a refresh.
        Future<String> secondResult = executorService.submit(() -> configSeederClient.getString("string.value"));

        // then
        assertThat(secondResult.get()).isEqualTo("val2");
    }

    @Test
    void shouldReadFromBackup() throws RequestTimedOutException, ConfigurationValueServerHttpStatusException {
        // given
        final ConfigSeederClientConfiguration clientConfig = ConfigSeederClientConfiguration.builder()
                .serverUrl("http://localhost/").apiKey("just-some-api-key")
                .initializationMode(InitializationMode.EAGER).refreshMode(RefreshMode.TIMER)
                .maxRetries(0)
                .recoveryFile("src/test/resources/.ConfigSeederClientTest.recovery.json")
                .requestConfiguration(ConfigurationRequest.builder()
                                              .configurations(singletonList(ConfigurationEntryRequest.builder().configurationGroupKey("CG")
                                                                                    .selectionMode(VersionedConfigurationGroupSelectionMode.LATEST)
                                                                                    .build())).build())
                .build();
        given(configSeederRestClient.getProperties(any(ConfigurationRequest.class)))
                .willAnswer(a -> { throw new RequestTimedOutException("simulate a timeout"); });
        given(configSeederRestClient.getRequestUrl()).willReturn("http://localhost");
        configSeederClient = new ConfigSeederClient(clientConfig, configSeederRestClient);

        // when
        Boolean recoveredValue = configSeederClient.getBoolean("from.recovery");

        // then
        assertThat(recoveredValue).isEqualTo(Boolean.TRUE);
    }


    private static ConfigValue cv(ConfigurationValueType valueType, String content) {
        final ConfigValue value = new ConfigValue();
        value.setKey(valueType.name());
        value.setValue(content);
        value.setType(valueType);
        return value;
    }

    @Test
    void shouldNotifyOnUpdate() {
        // given
        when(clientConfiguration.isDisabled()).thenReturn(true);
        configSeederClient = new ConfigSeederClient(clientConfiguration, configSeederRestClient);
        final Map<String, ConfigValue> oldValues = new HashMap<>();
        oldValues.put("keyA", createConfigValue("val1", PREVIOUS));
        oldValues.put("unchanged", createConfigValue("unchanged", PREVIOUS));
        final Map<String, ConfigValue> newValues = new HashMap<>();
        newValues.put("keyA", createConfigValue("val2", ZonedDateTime.now()));
        newValues.put("unchanged", createConfigValue("unchanged", PREVIOUS));

        final Map<String, KeyUpdate> updates = new HashMap<>();
        configSeederClient.subscribe(updates::putAll);

        // when
        configSeederClient.notifyUpdates(oldValues, newValues);

        // then
        assertThat(updates.size()).isEqualTo(1);
        assertThat(updates.get("keyA").getPreviousValue().getValue()).isEqualTo("val1");
        assertThat(updates.get("keyA").getNewValue().getValue()).isEqualTo("val2");
        assertThat(updates.get("keyA").isAddedValue()).isEqualTo(false);
        assertThat(updates.get("keyA").isDeletedValue()).isEqualTo(false);
    }

    @Test
    void shouldNotifyOnAdd() {
        // given
        when(clientConfiguration.isDisabled()).thenReturn(true);
        configSeederClient = new ConfigSeederClient(clientConfiguration, configSeederRestClient);
        final Map<String, ConfigValue> oldValues = emptyMap();
        final Map<String, ConfigValue> newValues = singletonMap("keyA", createConfigValue("val2", ZonedDateTime.now()));

        final Map<String, KeyUpdate> updates = new HashMap<>();
        configSeederClient.subscribe(updates::putAll);

        // when
        configSeederClient.notifyUpdates(oldValues, newValues);

        // then
        assertThat(updates.size()).isEqualTo(1);
        assertThat(updates.get("keyA").getPreviousValue()).isNull();
        assertThat(updates.get("keyA").getNewValue().getValue()).isEqualTo("val2");
        assertThat(updates.get("keyA").isAddedValue()).isEqualTo(true);
        assertThat(updates.get("keyA").isDeletedValue()).isEqualTo(false);
    }

    @Test
    void shouldNotifyOnDelete() {
        // given
        when(clientConfiguration.isDisabled()).thenReturn(true);
        configSeederClient = new ConfigSeederClient(clientConfiguration, configSeederRestClient);
        final Map<String, ConfigValue> oldValues = singletonMap("keyA", createConfigValue("val1", PREVIOUS));
        final Map<String, ConfigValue> newValues = emptyMap();

        final Map<String, KeyUpdate> updates = new HashMap<>();
        configSeederClient.subscribe(updates::putAll);

        // when
        configSeederClient.notifyUpdates(oldValues, newValues);

        // then
        assertThat(updates.size()).isEqualTo(1);
        assertThat(updates.get("keyA").getPreviousValue().getValue()).isEqualTo("val1");
        assertThat(updates.get("keyA").getNewValue()).isNull();
        assertThat(updates.get("keyA").isAddedValue()).isEqualTo(false);
        assertThat(updates.get("keyA").isDeletedValue()).isEqualTo(true);
    }

    private static ConfigValue createConfigValue(String value, ZonedDateTime lastChange) {
        final ConfigValue configValue = new ConfigValue();
        configValue.setValue(value);
        configValue.setLastUpdateInMilliseconds(lastChange.toInstant().toEpochMilli());
        configValue.setLastZonedUpdate(lastChange);
        return configValue;
    }

    private ConfigSeederClientConfiguration buildClientConfig() {
        return ConfigSeederClientConfiguration.builder()
                .serverUrl("http://localhost/")
                .apiKey("just-some-api-key")
                .initializationMode(InitializationMode.EAGER)
                .refreshMode(RefreshMode.MANUAL)
                .requestConfiguration(ConfigurationRequest.builder()
                                              .configurations(singletonList(ConfigurationEntryRequest.builder()
                                                                                    .configurationGroupKey("CG")
                                                                                    .selectionMode(VersionedConfigurationGroupSelectionMode.LATEST)
                                                                                    .build()))
                                              .build())
                .build();
    }

}