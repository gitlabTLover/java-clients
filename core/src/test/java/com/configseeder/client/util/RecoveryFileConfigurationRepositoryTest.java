/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client.util;

import static com.configseeder.client.util.RecoveryFileConfigurationRepository.DEFAULT_FILENAME;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyCollection;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;

import com.configseeder.client.model.ConfigValue;
import com.configseeder.client.model.ConfigurationValueType;
import java.io.File;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RecoveryFileConfigurationRepositoryTest {

    @Test
    void shouldCreateDefaultFileInFolder(@Mock JsonValueMapper mapper) {
        // given
        String directory = "target/tmp2/";
        File expectedFile = new File(directory + DEFAULT_FILENAME);

        // when
        RecoveryFileConfigurationRepository repository = new RecoveryFileConfigurationRepository(mapper);
        repository.setRecoveryFilePath(directory);

        // then
        assertThat(expectedFile).exists();
    }

    @Test
    void shouldCreateFileIfItDoesNotExists(@Mock JsonValueMapper mapper) {
        // given
        String filename = "target/tmp1/.recovery-file.json";
        testCreateFile(mapper, filename);
    }

    @Test
    void shouldCreateFileIfItDoesNotExistsSameDirectory(@Mock JsonValueMapper mapper) {
        // given
        String filename = ".recovery-file.json";
        testCreateFile(mapper, filename);
    }

    private void testCreateFile(JsonValueMapper mapper, String filename) {
        File file = new File(filename);

        // when
        RecoveryFileConfigurationRepository repository = new RecoveryFileConfigurationRepository(mapper);
        repository.setRecoveryFilePath(filename);

        // then
        assertThat(file).exists();
    }

    @Test
    void shouldStoreAndRetrieveContents() {
        // given
        ConfigValue value1 = new ConfigValue();
        value1.setKey("key.b");
        value1.setValue("value-b");
        value1.setType(ConfigurationValueType.STRING);
        value1.setLastUpdateInMilliseconds(135);
        value1.setLastZonedUpdate(ZonedDateTime.of(2019, 5, 21, 17, 0, 5, 0, ZoneId.systemDefault()));
        ConfigValue value2 = new ConfigValue();
        value2.setKey("key.c");
        value2.setValue("value-c");
        value2.setType(ConfigurationValueType.STRING);
        value2.setLastUpdateInMilliseconds(42);
        value2.setLastZonedUpdate(ZonedDateTime.of(2020, 5, 21, 17, 0, 5, 0, ZoneId.systemDefault()));

        JsonValueMapper jsonValueMapper = new JsonValueMapper();
        RecoveryFileConfigurationRepository repository = new RecoveryFileConfigurationRepository(jsonValueMapper);
        repository.setRecoveryFilePath("target/tmp3/");

        // when
        repository.saveConfigValues(singletonList(value2));
        Map<String, ConfigValue> persistedValues = repository.getPersistedValues();

        // then
        assertThat(persistedValues).hasSize(1);
        assertThat(persistedValues.get("key.c")).isEqualTo(value2);

        // when
        repository.saveConfigValues(singletonList(value1));
        persistedValues = repository.getPersistedValues();

        // then
        assertThat(persistedValues).hasSize(1);
        assertThat(persistedValues.get("key.b")).isEqualTo(value1);

        // when
        repository.saveConfigValues(emptyList());
        persistedValues = repository.getPersistedValues();

        // then
        assertThat(persistedValues).hasSize(0);

        // when
        repository.saveConfigValues(asList(value1, value2));
        persistedValues = repository.getPersistedValues();

        // then
        assertThat(persistedValues.get("key.b")).isEqualTo(value1);
        assertThat(persistedValues.get("key.c")).isEqualTo(value2);
    }

    @Test
    void shouldNotSaveAndRetrieveAnythingIfRecoveryFileIsNotDefined(@Mock JsonValueMapper jsonValueMapper, @Mock ConfigValue configValue) {
        // given
        RecoveryFileConfigurationRepository repository = new RecoveryFileConfigurationRepository(jsonValueMapper);

        // when
        Map<String, ConfigValue> values = repository.getPersistedValues();

        // then
        assertThat(values).isEmpty();

        // when
        repository.saveConfigValues(singletonList(configValue));

        // then
        then(jsonValueMapper).should(never()).write(anyCollection());
    }

}