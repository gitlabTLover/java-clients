/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class ConfigTest {

    @BeforeAll
    static void setup() {
        Config.setupCustomInstance("/ConfigTest.properties", "/ConfigTest.properties");
    }

    @Test
    void shouldReadExisting() {
        assertThat(Config.getString("existing.string")).isEqualTo("string");
        assertThat(Config.getLong("existing.long")).isEqualTo(5L);
        assertThat(Config.getBoolean("existing.boolean")).isEqualTo(Boolean.TRUE);
    }

    @Test
    void shouldReadOptionalExisting() {
        assertThat(Config.getOptionalString("existing.string").orElse(null)).isEqualTo("string");
        assertThat(Config.getOptionalLong("existing.long").orElse(null)).isEqualTo(5L);
        assertThat(Config.getOptionalBoolean("existing.boolean").orElse(null)).isEqualTo(Boolean.TRUE);
    }

    @Test
    void shouldReadOptionalNotExisting() {
        assertThat(Config.getOptionalString("notexisting.string").isPresent()).isEqualTo(false);
        assertThat(Config.getOptionalLong("notexisting.long").isPresent()).isEqualTo(false);
        assertThat(Config.getOptionalBoolean("notexisting.boolean").isPresent()).isEqualTo(false);
    }

    @Test
    void shouldReadFallback() {
        assertThat(Config.getString("notexisting.string", "fallback")).isEqualTo("fallback");
        assertThat(Config.getLong("notexisting.long", 42L)).isEqualTo(42L);
        assertThat(Config.getBoolean("notexisting.boolean", Boolean.TRUE)).isEqualTo(Boolean.TRUE);
    }

}