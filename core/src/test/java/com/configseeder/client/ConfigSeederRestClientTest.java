/*
 * Copyright (c) 2019 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client;

import static com.configseeder.client.ConfigSeederRestClient.APPLICATION_JSON_METADATA;
import static com.configseeder.client.ConfigSeederRestClient.HEADER_ACCEPT;
import static com.configseeder.client.ConfigSeederRestClient.HEADER_AUTHORIZATION;
import static com.configseeder.client.ConfigSeederRestClient.HEADER_CONTENT_TYPE;
import static com.configseeder.client.ConfigSeederRestClient.PATH_TO_EXTERNAL_PROPERTIES;
import static com.configseeder.client.model.Identification.HEADER_X_HOSTNAME;
import static com.configseeder.client.model.Identification.HEADER_X_HOST_IDENTIFICATION;
import static com.configseeder.client.model.Identification.HEADER_X_USER_AGENT;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.containing;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

import com.configseeder.client.model.ConfigValue;
import com.configseeder.client.model.ConfigurationEntryRequest;
import com.configseeder.client.model.ConfigurationRequest;
import com.configseeder.client.model.Identification;
import com.configseeder.client.model.VersionedConfigurationGroupSelectionMode;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.Options;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@Slf4j
@ExtendWith(MockitoExtension.class)
class ConfigSeederRestClientTest {

    @Test
    void shouldFailForMissingParameters() {
        assertThrows(IllegalArgumentException.class,
                     () -> new ConfigSeederRestClient("localhost:8080", "api-key"));
        assertThrows(IllegalArgumentException.class,
                     () -> new ConfigSeederRestClient("https://localhost:8080", " "));
    }

    @Test
    void shouldSendRequestWithAllParams(@Mock Identification identification) throws RequestTimedOutException, ConfigurationValueServerHttpStatusException {
        WireMockServer wireMockServer = new WireMockServer(Options.DYNAMIC_PORT);
        wireMockServer.start();
        log.info("Server running on port " + wireMockServer.port());

        // given
        given(identification.getUserAgent()).willReturn("ConfigSeederClientTest");
        given(identification.getHostname()).willReturn("jupiter");
        given(identification.getHostIdentification()).willReturn(UUID.randomUUID().toString());

        final ConfigSeederRestClient restClient = new ConfigSeederRestClient("http://localhost:" + wireMockServer.port(), "some-api-key", 5000, 5000,
                                                                             identification);
        final ConfigurationRequest request = ConfigurationRequest.builder()
                .tenantKey("test-tenant")
                .environmentKey("prod")
                .version("1.9.0")
                .dateTime(LocalDateTime.of(2019, 2, 15, 7, 5, 0))
                .context("development")
                .configurations(singletonList(ConfigurationEntryRequest.builder()
                                                      .selectionMode(VersionedConfigurationGroupSelectionMode.LATEST)
                                                      .configurationGroupKey("groupKey").build()))
                .build();

        wireMockServer.stubFor(post(urlEqualTo(PATH_TO_EXTERNAL_PROPERTIES))
                                       .willReturn(aResponse()
                                                           .withStatus(200)
                                                           .withHeader("Content-Type", "application/json")
                                                           .withBody(
                                                                   "[{\"key\": \"foo.bar\", \"value\": \"any text\", \"type\": \"STRING\", "
                                                                           + "\"lastUpdate\":\"2018-01-03T17:36:01\", \"lastUpdateInMilliseconds\": 125455545445}]")
                                                  )
                              );
        log.info("Request config defined as " + request);
        log.info("Call server on URL " + restClient.getRequestUrl());

        // when
        final Map<String, ConfigValue> configurationValues = restClient.getProperties(request);

        // then
        assertThat(configurationValues.get("foo.bar").getValue()).isEqualTo("any text");
        wireMockServer.verify(postRequestedFor(urlEqualTo(PATH_TO_EXTERNAL_PROPERTIES))
                .withHeader(HEADER_ACCEPT, equalTo(APPLICATION_JSON_METADATA))
                .withHeader(HEADER_CONTENT_TYPE, containing(APPLICATION_JSON_METADATA))
                .withHeader(HEADER_AUTHORIZATION, equalTo("Bearer some-api-key"))
                .withHeader(HEADER_X_USER_AGENT, equalTo("ConfigSeederClientTest"))
                .withHeader(HEADER_X_HOSTNAME, equalTo("jupiter"))
                .withHeader(HEADER_X_HOST_IDENTIFICATION, equalTo(identification.getHostIdentification()))
              );

        wireMockServer.stop();
    }

}