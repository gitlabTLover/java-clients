/*
 * Copyright (c) 2019 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Any form of reproduction, distribution, exploitation or alteration is prohibited
 * without the prior written consent of Oneo GmbH.
 */

package com.configseeder.demo.spring;

import com.configseeder.client.ConfigSeederClient;
import java.util.Map;
import java.util.TreeMap;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
@RequiredArgsConstructor
public class DemoConfigurationController {

    private final Environment environment;
    private final ConfigSeederClient configSeederClient;

    @Value("${mail.host:unknown}")
    private String staticMailHostValue;

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("greeting", "Welcome to Config Seeder");
        model.addAttribute("titleBackgroundColor", getBackgroundColor());
        return "index";
    }

    @GetMapping(path = "all-keys")
    public ResponseEntity<Map<String, String>> getAllKeys() {
        Map<String, String> values = new TreeMap<>();
        configSeederClient.getKeys().forEach(v -> values.put(v, environment.getProperty(v)));
        return new ResponseEntity<>(values, HttpStatus.OK);
    }

    @GetMapping(path = "specific/{key:.+}")
    public ResponseEntity<?> getValue(@PathVariable(name = "key") String key) {
        final String configurationValue = environment.getProperty(key);
        return configurationValue == null ? ResponseEntity.notFound().build() : ResponseEntity.ok(configurationValue);
    }

    @GetMapping(path = "mail.host")
    public ResponseEntity<String> getMailHost() {
        return ResponseEntity.ok(staticMailHostValue);
    }

    private String getBackgroundColor() {
        String backgroundColor = environment.getProperty("page.title.background.color");
        if (backgroundColor == null) {
            backgroundColor = "#ffffff";
        }
        return backgroundColor;
    }

}
