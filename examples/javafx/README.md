# ConfigSeeder Java Core Example based on a small JavaFX Application

## Start

* Compile with Maven
  * `mvn clean install`
  * java -jar configseeder-demo-app-*.jar
* Or run from your IDE and start main class `DemoFxClient`

## Configurations

- Configured with `configseeder.yaml`

Following configuration properties are used:

- `javafx.app.title`
- `javafx.app.width`
- `javafx.pane.background`
- `javafx.table.background`
