/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.demo.fx;

import com.configseeder.client.Config;
import com.configseeder.client.ConfigSeederClient;
import com.configseeder.client.ConfigSeederClientConfiguration;
import com.configseeder.client.model.ConfigValue;
import com.configseeder.client.model.Identification;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;


public class DemoFxClient extends Application {

    private final TableView<ConfigValue> table = new TableView<>();
    private VBox vbox;
    private ConfigSeederClient configSeederClient;
    private ObservableList<ConfigValue> data;
    private Stage stage;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stageParam) {
        configSeederClient = setupConfigSeederClient();
        Config.init(configSeederClient);
        stage = stageParam;

        refreshValues();

        final Label label = new Label("Configurations");
        label.setFont(new Font("Arial", 20));
        table.setEditable(false);

        TableColumn<ConfigValue, String> keyColumn = new TableColumn<>("Key");
        keyColumn.setCellValueFactory(new PropertyValueFactory<>("key"));

        TableColumn<ConfigValue, String> valueColumn = new TableColumn<>("Value");
        valueColumn.setCellValueFactory(new PropertyValueFactory<>("value"));

        TableColumn<ConfigValue, String> lastUpdateColumn = new TableColumn<>("Last Update");
        lastUpdateColumn.setCellValueFactory(new PropertyValueFactory<>("lastZonedUpdate"));

        final ObservableList<TableColumn<ConfigValue, ?>> columns = table.getColumns();
        columns.addAll(keyColumn, valueColumn, lastUpdateColumn);
        table.setItems(data);
        table.setMaxWidth(Double.MAX_VALUE);

        Button refresh = new Button("Manual Refresh");
        refresh.setOnAction(e -> {
            configSeederClient.forcedRefresh();
            refreshValues();
            updateDynamicValues();
            table.refresh();
        });

        final HBox hbox = new HBox();
        hbox.setSpacing(5);
        hbox.setFillHeight(true);
        hbox.getChildren().addAll(label, refresh);

        vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 10, 10, 10));
        vbox.setFillWidth(true);
        vbox.getChildren().addAll(hbox, table);

        Scene scene = new Scene(vbox);
        stage.setTitle("JavaFX Config Client");
        stage.setWidth(700);
        stage.setHeight(500);
        stage.setScene(scene);
        stage.show();
        updateDynamicValues();

        final Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(1), event -> {
            refreshValues();
            updateDynamicValues();
            table.refresh();
        }));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    private void updateDynamicValues() {
        // Table and vbox background example
        vbox.setBackground(new Background(new BackgroundFill(getConfiguredColor("javafx.pane.background", Color.TRANSPARENT), CornerRadii.EMPTY, Insets.EMPTY)));
        table.setBackground(new Background(new BackgroundFill(getConfiguredColor("javafx.table.background", Color.WHITE), CornerRadii.EMPTY, Insets.EMPTY)));

        // Fallback Example
        stage.setTitle(Config.getString("javafx.app.title", "JavaFX Config Client"));


        // Optional Example
        Config.getOptionalLong("javafx.app.width").ifPresent(aLong -> stage.setWidth(aLong.doubleValue()));
    }

    private Color getConfiguredColor(String colorKey, Color fallback) {
        final String configColor = configSeederClient.getString(colorKey);
        final Color color;
        if (configColor != null && configColor.matches("#[0-9A-Fa-f]{6}")) {
            color = Color.web(configColor);
        } else {
            color = fallback;
        }
        return color;
    }

    private ConfigSeederClient setupConfigSeederClient() {
        final Identification identification = Identification.create("ConfigSeederDemo/JavaFx");
        final String configSeederFile = System.getProperty("configseeder.client.file");
        if (configSeederFile != null && Files.exists(Paths.get(configSeederFile))) {
            try {
                return new ConfigSeederClient(ConfigSeederClientConfiguration.fromYaml(Files.newInputStream(Paths.get(configSeederFile))), identification);
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }
        return new ConfigSeederClient(ConfigSeederClientConfiguration.fromYaml("/configseeder.yaml"), identification);
    }

    private void refreshValues() {
        final Map<String, ConfigValue> allValues = configSeederClient.getValues();
        if (data == null) {
            final ConfigValue[] values = allValues.values().toArray(new ConfigValue[0]);
            data = FXCollections.observableArrayList(values);
        } else {
            final Iterator<ConfigValue> iterator = data.iterator();
            while (iterator.hasNext()) {
                final ConfigValue next = iterator.next();
                final ConfigValue nextValue = allValues.get(next.getKey());
                if (nextValue == null) {
                    iterator.remove();
                } else {
                    next.setValue(nextValue.getValue());
                    next.setLastUpdateInMilliseconds(nextValue.getLastUpdateInMilliseconds());
                    next.setLastZonedUpdate(nextValue.getLastZonedUpdate());
                }
            }
            allValues.forEach((key, value) -> {
                if (data.stream().noneMatch(v -> v.getKey().equals(key))) {
                    data.add(value);
                }
            });
        }

        data.sort(Comparator.comparing(ConfigValue::getKey));
    }
}
