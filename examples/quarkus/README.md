# ConfigSeeder Integration with Quarkus

This example shows you how to integrate ConfigSeeder® with your Quarkus / Eclipse Microprofile based application.

## Start

* `mvn compile quarkus:dev`
* Server runs on Port `8081`.

## Democase

This demo case shows how ConfigSeeder® can easily be integrated into your Eclipse Microprofile environment:

1. Include dependency to ConfigSeeder® in your `pom.xml`:
  
   ```xml
   <dependency>
     <groupId>com.configseeder.client</groupId>
     <artifactId>configseeder-client-smallrye-config</artifactId>
   </dependency>
   ```

2. Configure ConfigSeeder® client with a config file in `/src/main/resources/application.properties`.
   Relevant configurations: `configseeder.client.serverUrl`, `configseeder.client.apiKey`, `configseeder.client.configurationGroupKeys` and `configseeder.client.environmentKey`. In a typical production scenario, `apiKey` and `environmentKey` should be the only configurations that should be not committed to the repository, but passed as environment variables.
   
   1. Use a user defined on https://configseeder.com/demo/ and login to our demo instance on https://demo.configseeder.com/.
   1. Create an own configuration group
   1. Create an API key
   1. Update configuration and set `tenantKey` to `demo`.
   1. Update configuration values `apiKey`, `configurationGroupKeys` (and if you wish also `environmentKey`).

3. Use standard Eclipse Microprofile Config annotations to inject configurations:

   ```java
    @Inject
    @ConfigProperty(name = "mail.from", defaultValue = "fallback")
    String staticValue;

    @ConfigProperty(name = "mail.host", defaultValue = "https://mail.google.com")
    Provider<String> mailHost;
    ``` 

## REST Endpoints

(see `QuarkusConfigurationResource`)

- `GET /all-keys/` => Shows all Keys
- `GET /mail-settings` => Shows contents of key `mail.from` (not changeable at runtime) and `mail.host` (changeable at runtime)
- `GET /specific/<key>/` => Shows the configuration value for the given key. (WARNING: Never do such thing in production!)

