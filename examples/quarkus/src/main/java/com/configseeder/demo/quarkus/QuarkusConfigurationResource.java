/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.demo.quarkus;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@Path("/")
public class QuarkusConfigurationResource {

    @Inject
    @ConfigProperty(name = "mail.from", defaultValue = "fallback")
    String staticValue;

    @ConfigProperty(name = "mail.host", defaultValue = "https://mail.google.com")
    Provider<String> mailHost;

    @ConfigProperty(name = "mail.to")
    Provider<Optional<String>> mailTo;

    @Inject
    Config config;

    /**
     * All existing configuration keys.
     *
     * @return list of all existing configuration keys
     */
    @GET
    @Path("all-keys")
    @Produces("application/json")
    public Response allKeys() {
        final Iterable<String> propertyNames = config.getPropertyNames();
        final List<String> keys = StreamSupport.stream(propertyNames.spliterator(), false).collect(toList());
        return Response.ok(keys).build();
    }

    /**
     * Mail Settings based on properties mail.from and mail.to.
     *
     * @return mail settings as JSON
     */
    @GET
    @Path("mail-settings")
    @Produces("application/json")
    public Response getConfigurations() {
        return Response.ok(new MailSettings(staticValue, mailHost.get(), mailTo.get().orElse(null))).build();
    }

    /**
     * Exposes all configurations with this key.
     * <p>
     * WARNING: NEVER DO THIS IN PRODUCTION! YOU MAY LEAK SENSITIVE CONFIGURATIONS.
     * </p>
     *
     * @param key configuration key
     * @return response
     */
    @GET
    @Path("specific/{key}")
    public Response specificKey(@PathParam("key") String key) {
        final Optional<String> value = config.getOptionalValue(key, String.class);
        if (value.isPresent()) {
            return Response.ok(value.get()).build();
        } else {
            return Response.status(Status.NOT_FOUND).build();
        }
    }

}
