/*
 * Copyright (c) 2019 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client.spring;

import com.configseeder.client.ConfigSeederClient;
import com.configseeder.client.ConfigSeederClientConfiguration;
import com.configseeder.client.InvalidClientConfigurationException;
import com.configseeder.client.Logger;
import com.configseeder.client.model.Identification;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.lang.NonNull;

/**
 * Initialize config seeder client at spring startup.
 */
public class ConfigSeederStartupInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Override
    public void initialize(@NonNull ConfigurableApplicationContext applicationContext) {
        if (ConfigSeederStartupInitializer.class.getResourceAsStream("/configseeder.yaml") != null) {
            final ConfigSeederClientConfiguration configSeederClientConfiguration = ConfigSeederClientConfiguration
                    .fromYaml("/configseeder.yaml");
            try {
                configSeederClientConfiguration.validate();

                ConfigSeederClient configSeederClient = new ConfigSeederClient(configSeederClientConfiguration, Identification.create("Spring"));
                initializePropertySource(applicationContext, configSeederClient);

                final ConfigurableListableBeanFactory beanFactory = applicationContext.getBeanFactory();
                beanFactory.registerSingleton("configSeederClient", configSeederClient);

                initializeContextRefreshEvent(applicationContext, configSeederClient);
            } catch (InvalidClientConfigurationException e) {
                Logger.getLogger(this.getClass()).warn(
                        "Incomplete configuration to setup ConfigSeeder client. No values will be retrieved from ConfigSeeder. Message: {}", e.getMessage());
            }
        } else {
            Logger.getLogger(this.getClass()).warn("Unable to locate /configseeder.yaml - Skipping");
        }
    }

    private void initializePropertySource(ConfigurableApplicationContext applicationContext, ConfigSeederClient configProvider) {
        final ConfigSeederPropertySource configSeederPropertySource = new ConfigSeederPropertySource(configProvider);
        final ConfigurableEnvironment environment = applicationContext.getEnvironment();
        final MutablePropertySources propertySources = environment.getPropertySources();
        propertySources.addLast(configSeederPropertySource);
    }

    private void initializeContextRefreshEvent(ConfigurableApplicationContext applicationContext, ConfigSeederClient configProvider) {
        final ConfigurableEnvironment environment = applicationContext.getEnvironment();
        final Boolean contextRefreshEnabled = environment.getProperty("configseeder.client.enablecontextrefresh", Boolean.class);
        if (Boolean.TRUE.equals(contextRefreshEnabled)) {
            configProvider.subscribe(keys -> applicationContext.refresh());
        }
    }

}

