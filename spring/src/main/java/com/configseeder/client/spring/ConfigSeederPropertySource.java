/*
 * Copyright (c) 2019 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client.spring;

import com.configseeder.client.ConfigSeederClient;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.lang.NonNull;

/**
 * Property source based on config seeder.
 */
public class ConfigSeederPropertySource extends EnumerablePropertySource<ConfigSeederClient> {

    public ConfigSeederPropertySource(ConfigSeederClient source) {
        super("config-seeder", source);
    }

    public ConfigSeederPropertySource(String name, ConfigSeederClient source) {
        super(name, source);
    }

    @Override
    public String[] getPropertyNames() {
        return getSource().getKeys().toArray(new String[0]);
    }

    @Override
    public Object getProperty(@NonNull String name) {
        return getSource().getString(name);
    }

}
