/*
 * Copyright (c) 2021 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client.microprofileconfig;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.assertj.core.api.Assertions.assertThat;

import com.configseeder.client.ConfigSeederClient;
import com.configseeder.client.ConfigSeederClientConfiguration;
import com.configseeder.client.InvalidClientConfigurationException;
import com.configseeder.client.model.Identification;
import com.github.tomakehurst.wiremock.WireMockServer;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.spi.ConfigProviderResolver;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class ConfigSeederEclipseMicroProfileConfigSourceTest {

    private static WireMockServer wireMockServer;
    private ConfigSeederEclipseMicroProfileConfigSource configSource;

    @BeforeAll
    static void startServer() {
        // given
        wireMockServer = new WireMockServer(8091);
        wireMockServer.stubFor(post(urlEqualTo("/public/api/v1/configurations"))
                                     .willReturn(aResponse()
                                                         .withStatus(200)
                                                         .withHeader("Content-Type", "application/json")
                                                         .withBody("[{\"key\": \"foo.bar\", \"value\": \"any text\", \"type\": \"STRING\", "
                                                                           + "\"lastUpdate\":\"2018-01-03T17:36:01\", "
                                                                           + "\"lastUpdateInMilliseconds\": 101837483273}]")
                                                )
                            );
        wireMockServer.start();
    }

    @AfterEach
    void shutdownAccess() {
        if (configSource != null) {
            configSource.close();
            configSource = null;
        }
    }

    @AfterAll
    static void stopServer() {
        wireMockServer.stop();
    }

    @Test
    void shouldLoadValueWithoutMicroProfile() {
        // given
        configSource = new ConfigSeederEclipseMicroProfileConfigSource();

        // when
        final String value = configSource.getValue("foo.bar");

        // then
        assertThat(value).isEqualTo("any text");
    }

    @Test
    void shouldProvideValueMicroProfileWithoutSpi() {
        // given
        final Config config = ConfigProviderResolver.instance().getBuilder()
                .addDefaultSources()
                .addDiscoveredSources()
                .build();

        // when
        final String value = config.getValue("foo.bar", String.class);

        // then
        assertThat(value).isEqualTo("any text");
    }

    @Test
    void shouldInitializeWithEclipseMicroProfileConfig() {
        // given
        final ConfigSeederClientConfiguration clientConfiguration = new ConfigSeederClientConfigurationProvider().createConfigurationFromMicroProfileConfig();
        final ConfigSeederClient client = new ConfigSeederClient(clientConfiguration, Identification.create("TST"));
        configSource = new ConfigSeederEclipseMicroProfileConfigSource(client);

        // when
        final String value = configSource.getValue("foo.bar");

        // then
        assertThat(value).isEqualTo("any text");
    }

    @Test
    void shouldInitializeWithInvalidConfig() {
        // given
        configSource = new ConfigSeederEclipseMicroProfileConfigSource() {
            @Override
            protected ConfigSeederClient getConfigSeederClient() {
                throw new InvalidClientConfigurationException("Invalid configuration");
            }
        };

        // when
        final String value = configSource.getValue("foo.bar");

        // then
        assertThat(value).isNull();
    }

}
