/*
 * Copyright (c) 2021 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client.microprofileconfig;

import static com.configseeder.client.ConfigSeederClientConfiguration.ALL_CONFIG_KEYS;

import com.configseeder.client.ConfigSeederClientConfiguration;
import java.util.Map;
import java.util.logging.Level;
import java.util.stream.Collectors;
import lombok.extern.java.Log;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.ConfigProvider;

@Log
public class ConfigSeederClientConfigurationProvider extends ConfigSeederClientBaseConfigurationProvider {

    public ConfigSeederClientConfiguration getClientConfiguration() {
        if (ConfigSeederClientConfigurationProvider.class.getResourceAsStream("/configseeder.yaml") != null) {
            return createConfigurationFromYaml();
        } else {
            return createConfigurationFromMicroProfileConfig();
        }
    }

    public ConfigSeederClientConfiguration createConfigurationFromMicroProfileConfig() {
        log.log(Level.FINER, "Create ConfigSeederClientConfiguration based on ConfigSourceContext");
        final Config config = ConfigProvider.getConfig();
        final Map<Object, Object> existingConfigs = ALL_CONFIG_KEYS.stream()
                .map(k -> new KeyValue(k, config.getOptionalValue(k, String.class).orElse(null)))
                .filter(p -> p.getValue() != null)
                .collect(Collectors.toMap(KeyValue::getKey, KeyValue::getValue));
        return ConfigSeederClientConfiguration.fromMap(existingConfigs);
    }

}
