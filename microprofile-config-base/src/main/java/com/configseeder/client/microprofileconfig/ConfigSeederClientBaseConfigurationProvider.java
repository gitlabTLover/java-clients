/*
 * Copyright (c) 2021 Oneo GmbH (ConfigSeeder®, https://configseeder.com/) and others.
 *
 * Any form of reproduction, distribution, exploitation or alteration is prohibited
 * without the prior written consent of Oneo GmbH.
 */

package com.configseeder.client.microprofileconfig;

import com.configseeder.client.ConfigSeederClientConfiguration;
import java.util.logging.Level;
import lombok.extern.java.Log;

@Log
public class ConfigSeederClientBaseConfigurationProvider {

    public ConfigSeederClientConfiguration createConfigurationFromYaml() {
        log.log(Level.FINER, "Create ConfigSeederClientConfiguration from classpath:/configseeder.yaml");
        return ConfigSeederClientConfiguration.fromYaml("/configseeder.yaml");
    }

}
